<?php

namespace app\controllers;

use app\models\MateOrder;
use app\models\Payment;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

class MateOrderController extends \yii\web\Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout',  'index'],
                'rules' => [
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }
    public function actionIndex()
    {
        return $this->render('index');
    }
    public function actionView($id)
    {
        $model = $this->findModel($id);
        return $this->render('view',['model'=>$model]);
    }
    public function actionSavePayment()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        if($post = \Yii::$app->request->post()){
            $data = $post['item'];
             $model = $this->findModel($data['mate_order_id']);
            $payment = $post['item']['payment'];

            $sPayment = new Payment();
            $sPayment->status = 1;
            $sPayment->mate_order_id = $model->id; // รหัสสั่งทำ
            $sPayment->type_payment = 2;  // ประเภทสั่งทำ
            $sPayment->bank_id = intval($post['item']['bank_id']);
            $sPayment->check_time = $payment['hours'] . ':' . $payment['time'];
            $sPayment->remark = $payment['remark'];
            $sPayment->save();
            $model->status = 2;
            $model->save();

            $message = "\n " . ' ทำการชำระมัดจำสินค้าแล้ว';
            $message .= "\n ช่วงเวลา " . $payment['hours'] . ':' . $payment['time'];
            $message .= "\n ราคามัดจำ " . $model->deposit_price . ' บาท';
            $message .= "\n ตรวจรายการ " . 'http://' . $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . '/mate-order/confirm?id=' . $model->id;
            \Yii::$app->global->actionSendLine($message);
          return [
              'data' => 'ok'
          ];

        }else{
            return [
                'data' => 'not'
            ];
        }
    }
    public function actionConfirm($id){
        $model = $this->findModel($id);
        $model->status = 3 ;
        $model->save();
        $p = Payment::findOne(['mate_order_id'=>$model->id]);
        $p->status = 2;
        $p->save();
        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = MateOrder::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
