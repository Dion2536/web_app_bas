<?php

namespace app\controllers;

use app\models\Address;
use app\models\Movement;
use app\models\Photos;
use app\models\Product;
use app\models\Profile;
use app\models\SignupForm;
use app\models\Types;
use app\models\User;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use kartik\widgets\Alert;
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'items-details', 'profile'],
                'rules' => [
                    [
                        'actions' => ['logout', 'items-details', 'profile'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        if (!Yii::$app->user->isGuest){
            if (!Profile::findOne(['user_id'=>Yii::$app->user->id])->full_name) {
                return $this->redirect(['site/profile?show=profile']);
            }
        }


        $search = Yii::$app->request->get('search');
        if (isset($search)) {
            $dataProvider = Yii::$app->db->createCommand("select * from product where name LIKE '%$search%' order by id desc ")->queryAll();

        } else {
            $dataProvider = Yii::$app->db->createCommand('select * from product where status = 1 order by id desc ')->queryAll();
        }

        foreach ($dataProvider as $k => &$model) {
            $model['photos'] = $dataPH = Photos::findOne(['product_id' => $model['id']]) ? Yii::getAlias('@web') . '/uploads/' . Photos::findOne(['product_id' => $model['id']])->name : '';
        }
        $product_type = Types::find()->where(['status' => 1])->asArray()->all();
        foreach ($product_type as $k => &$type) {
            $type['count'] = Product::find()->where(['types_id' => $type['id']])->count();
        }
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'product_type' => json_encode($product_type)
        ]);
    }
    public function actionConfirm($id,$token){
       $u= User::findOne($id);
        $u->status = 10;
        $u->save();
        $this->redirect(['login']);
    }
    public function actionItemsDetails($id)
    {
        $model = Product::find()->where(['id' => $id])->asArray()->all();
        foreach ($model as $k => &$models) {
            $models['photos'] = Photos::findOne(['product_id' => $models['id'], 'status_use' => 1]) ? Yii::getAlias('@web') . '/uploads/' . Photos::findOne(['product_id' => $models['id'], 'status_use' => 1])->name : '';
            $models['amount'] = Movement::find()->where(['product_id' => $models['id']])->sum('amount') ? Movement::find()->where(['product_id' => $models['id']])->sum('amount') : 0;
        }
        $product_list = Product::find()->where(['types_id' => Product::findOne(['id' => $id])->types_id])->asArray()->all();

        foreach ($product_list as $k => &$list) {
            $list['photos'] = Photos::findOne(['product_id' => $list['id'], 'status_use' => 1]) ? Yii::getAlias('@web') . '/uploads/' . Photos::findOne(['product_id' => $list['id'], 'status_use' => 1])->name : '';
        }
        $images = Photos::find()->where(['product_id' => $id])->asArray()->all();
        return $this->render('_item_product_details', [
            'model' => json_encode($model),
            'imagesList' => json_encode($images),
            'product_list' => json_encode($product_list)
        ]);
    }

    public function actionListCart()
    {
        $address = Address::findOne(['status' => 1, 'user_id' => Yii::$app->user->id]);
        return $this->render('_list_cart', ['address' => $address]);
    }

    public function actionSubCart()
    {
        return $this->renderPartial('subscribe_cart');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            if (!Profile::findOne(['user_id'=>User::findOne(['username'=>$model->username])->id])->full_name) {
                return $this->redirect(['site/profile?show=profile']);
            } else {
                return $this->goBack();

            }
        };
        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post()) && $model->signup()) {

            return $this->redirect(['login','id'=>'confirm']);
        }
        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionProfile()
    {
        $user_id = Yii::$app->user->id;
        $profile = Profile::findOne(['id' => $user_id]);
        if ($profile->load(Yii::$app->request->post())) {
            $profile->save();
            return $this->refresh();
        }
        return $this->render('profile', ['profile' => $profile]);
    }

    public function actionDeleteAddress($id)
    {
        Address::findOne(['id' => $id])->delete();
        return $this->redirect(['site/profile', 'show' => 'address']);
    }

    public function actionSetStatusAddress($id)
    {

        foreach (Address::findAll(['user_id' => Yii::$app->user->id]) as $model) {
            $model->status = null;
            $model->save();
        }
        $add = Address::findOne(['id' => $id]);
        $add->status = 1;
        $add->save();
        return $this->redirect(['site/profile', 'show' => 'address']);
    }
}
