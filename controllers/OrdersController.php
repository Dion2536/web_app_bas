<?php

namespace app\controllers;

use app\models\Movement;
use app\models\Order;
use app\models\OrderDetails;
use app\models\Payment;
use Yii;
use yii\db\Query;
use yii\helpers\Json;
use yii\helpers\Url;

class OrdersController extends \yii\web\Controller
{
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionAddCart()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $data = [];
        if ($post = Yii::$app->request->post()) {
            $product_id = intval($post['item']['id']);
            $amountCart = intval($post['item']['amount_set']);
            $user_id = intval(Yii::$app->user->id);
            $checkBalance = Movement::find()->where(['product_id' => $product_id])->sum('amount');
            if (!$checkBalance) {
                $data = [
                    'data' => [],
                    'status' => 'not'
                ];
            } else {
                $dataCartDetails = OrderDetails::findOne(['status' => 1, 'product_id' => $product_id, 'created_by' => $user_id]);
                if (!empty($dataCartDetails)) {
                    $amountTotal = $dataCartDetails->amount + $amountCart;
                    $dataProduct = $this->actionCheckBalance($dataCartDetails['product_id']);
                    if ($amountTotal > intval($dataProduct['amount'])) {
                        $data = [
                            'status' => 'not-balance',
                            'data' => $dataProduct,
                        ];
                    } else {
                        $dataCartDetails->amount = $dataCartDetails->amount + $amountCart;
                        $dataCartDetails->save();
                        $data = [
                            'data' => $this->actionReturnProduct($user_id),
                            'status' => 'ok'
                        ];
                    }
                } else {
                    $orderCart = new OrderDetails();
                    $orderCart->status = 1;
                    $orderCart->product_id = $product_id;
                    $orderCart->amount = $amountCart;
//                    $orderCart->user_id = $user_id;
                    $orderCart->save();
                    $data = [
                        'data' => $this->actionReturnProduct($user_id),
                        'status' => 'ok'
                    ];
                }
            }
        }
        return $data;
    }

    public function actionRemoveOrder($id)
    {
        OrderDetails::findOne(['id' => $id])->delete();
        return 'Del success';
    }

    public function actionCheckBalance($id)
    {
        return Yii::$app->db->createCommand("SELECT sum(m.amount) as amount FROM movement m 
                        WHERE m.product_id =:product_id
                        GROUP BY m.product_id HAVING  sum(m.amount) > 0 
                    ")->bindValues(['product_id' => $id])->queryOne();
    }

    public function actionCheckCartOrder()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $user_id = intval(Yii::$app->user->id);
        $rawData = (new Query())
            ->select('ocd.*,p.*, ph.name as photos ,ocd.id as order_detail_id')
            ->from('order_details ocd')
            ->innerJoin('product p', 'ocd.product_id= p.id')
            ->innerJoin('photos ph', 'p.id = ph.product_id')
            ->where(['ocd.status' => 1, 'ocd.created_by' => $user_id, 'ph.status_use' => 1])->orderBy('ocd.id asc')
            ->all();
        foreach ($rawData as &$model) {
            $amount = Movement::find()->where(['product_id' => $model['product_id']])->sum('amount');
            $model['amountBalance'] = $amount;
        }

        if (!empty($rawData)) {
            $data = [
                'status' => 'ok',
                'data' => $rawData
            ];
        } else {
            $data = [
                'status' => 'not',
                'data' => []
            ];
        }
        return $data;
    }

    public function actionAddAmount()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $user_id = intval(Yii::$app->user->id);
        if ($post = Yii::$app->request->post()) {
            $orderDetails = OrderDetails::findOne(['id' => $post['item']['order_detail_id']]);
            $orderDetails->amount = intval($post['item']['amount_set']);
            $orderDetails->save();
            return 'ok';
        }
    }

    public function actionReturnProduct($user_id)
    {
        $rawData = (new Query())
            ->select('
        od.id as od_id,
        od.amount,
        od.product_id,
        od.created_by,
        od.status as status_ob, p.*,
        ph.name as photos ')
            ->from('order_details od')
            ->innerJoin('product p', 'od.product_id= p.id')
            ->innerJoin('photos ph', 'p.id = ph.product_id')
            ->where(['od.status' => 1, 'od.created_by' => $user_id, 'ph.status_use' => 1])->orderBy('od_id desc')
            ->all();
        return $rawData;
    }

    public function actionSaveCart()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $user_id = intval(Yii::$app->user->id);
        if ($post = Yii::$app->request->post()) {
            $dataCart = $post['item']['dataCart'];
            $payment = $post['item']['payment'];
            $order = new Order();
            $order->status = 1;
            if ($order->save()) {
                $sPayment = new Payment();
                $sPayment->status = 1;
                $sPayment->order_id = $order->id;
                $sPayment->type_payment = 1;
                $sPayment->bank_id = intval($post['item']['bank_id']);
                $sPayment->check_time = $payment['hours'] . ':' . $payment['time'];
                $sPayment->remark = $payment['remark'];
                $sPayment->save();

                $total = 0;
                foreach ($dataCart as $dc) {
                    $total += $dc['amount'] * $dc['price'];
                    $upOrderDetail = OrderDetails::findOne(['id' => $dc['order_detail_id']]);
                    $upOrderDetail->order_id = $order->id;
                    $upOrderDetail->status = 2;
                    $upOrderDetail->save();
                }
                $message = "\n order :" . $order->id . ' ทำการชำระสินค้าแล้ว';
                $message .= "\n ช่วงเวลา " . $payment['hours'] . ':' . $payment['time'];
                $message .= "\n ราคา " . $total . ' บาท';
                $message .= "\n ตรวจรายการ " . 'http://' . $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . '/orders/check-payment?id=' . $order->id;
                $this->actionSendLine($message);
            }
        }
        return [
            'data' => 'ok'
        ];
    }

    public function actionCheckPayment($id = null)
    {
        $order = Order::findOne(['id' => $id]);
        return $this->renderPartial('check-payment', ['order' => $order]);
    }

    public function actionConfirm($id = null)
    {
        $model = Payment::findOne(['order_id' => $id]);
        if (Movement::getCartMovement($model->order_id)) {
            return 'ok';
        }
    }

    public function actionNotConfirm($id = null)
    {
        if (Movement::getNotConfirm($id)) return 'ok';
    }

    public function actionSendLine($message = null)
    {
        $sToken = Yii::$app->params['token_lineNotify'];
        $chOne = curl_init();
        curl_setopt($chOne, CURLOPT_URL, "https://notify-api.line.me/api/notify");
        curl_setopt($chOne, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($chOne, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($chOne, CURLOPT_POST, 1);
        curl_setopt($chOne, CURLOPT_POSTFIELDS, "message=" . $message);
        $headers = array('Content-type: application/x-www-form-urlencoded', 'Authorization: Bearer ' . $sToken . '',);
        curl_setopt($chOne, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($chOne, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($chOne);
        //Result error
        if (curl_error($chOne)) {
            echo 'error:' . curl_error($chOne);
        }

        curl_close($chOne);
    }
}
