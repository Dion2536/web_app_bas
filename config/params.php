<?php
 use Mpdf\Config\FontVariables;
 use Mpdf\Config\ConfigVariables;
return [
    'adminEmail' => 'selasai2020@gmail.com',
    'senderEmail' => 'selasai2020@gmail.com',
    'supportEmail'=>'selasai2020@gmail.com',
    'senderName' => 'Example.com mailer',
    'token_lineNotify'=>'mBbRaQs2XPpKfxc0S7tz6l6cquwx7zoeOx36JlphWvW',
   'defaultConfig' => (new ConfigVariables())->getDefaults(),
    'defaultFontConfig' => (new FontVariables())->getDefaults(),
    'SetFontTHsarabun' => [
        'R' => 'THSarabunNew.ttf',
        'B' => 'THSarabunNew Bold.ttf',
    ],
    //'urlImg' => Yii::getAlias('@web/uploads/')
];
