<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppFrontendAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'app_frontend/css/bootstrap.css',
        'app_frontend/fonts/fontawesome/css/font-awesome.css',
        'app_frontend/fonts/fontawesome/css/fontawesome-all.css',
        'app_frontend/plugins/fancybox/fancybox.min.css',
        'app_frontend/plugins/owlcarousel/assets/owl.carousel.min.css',
        'app_frontend/plugins/owlcarousel/assets/owl.theme.default.css',
        'app_frontend/css/sweetalert2.min.css',
        'app_frontend/css/ui.css?v=1.0',
        'app_frontend/css/responsive.css',
    ];
    public $js = [
        'app_frontend/js/bootstrap.bundle.min.js',
        'app_frontend/plugins/fancybox/fancybox.min.js',
        'app_frontend/plugins/owlcarousel/owl.carousel.min.js',
        'app_frontend/js/script.js',
        'app_frontend/vue/axios.min.js',
       'app_frontend/vue/moment.js',
       'app_frontend/vue/numeral.min.js',
        'app_frontend/vue/vue.js',
        'app_frontend/vue/vuex.js',
        'app_frontend/vue/sweetalert2.min.js',
        'app_frontend/vue/store_app.js'

    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
