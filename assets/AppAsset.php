<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'http://fonts.googleapis.com/css?family=Abril+Fatface:300,400,500,600,700,800,900',
        'http://fonts.googleapis.com/css?family=Roboto:300,400,500,600,700,800,900',
        'http://fonts.googleapis.com/css?family=Poppins:400,300,400,400i,500,600,600i,700,800,900',
        'https://fonts.googleapis.com/icon?family=Material+Icons',
        'app/css/bootstrap.min.css?47228',
        //        'yii\bootstrap\BootstrapAsset',
        'app/css/font-awesome/css/font-awesome.min.css',
        'app/css/animate.min.css?47228',
        'app/css/common.scss.css?47228',
        'app/css/owl.carousel.min.css?47228',
        'app/css/owl.theme.default.css?47228',
        'app/css/slick.scss.css?47228',
        'app/css/jquery.mmenu.all.css?47228',
        'app/css/jquery.fancybox.css?47228',
        'app/css/jquery.ui.min.css?47228',
        'app/css/layout.scss.css?47228',
        'app/css/theme.scss.css?47228',
        'app/css/responsive.scss.css?47228'
    ];
    public $js = [
//        'app/js/jquery.2.2.4.min.js?47228',
        'app/js/lazysizes.js?47228',
        'app/js/vendor.js?47228',
        'app/js/history.js?47228',
        'app/js/currencies.js',
        'app/js/jquery.currencies.min.js?47228',
        'app/js/jquery.instafeed.min.js?47228',
        'app/js/jquery.owl.carousel.min.js?47228',
        'app/js/jquery.mmenu.all.min.js?47228',
        'app/js/jquery.sticky-kit.min.js?47228',
        'app/js/parallax.js?47228',
        'app/js/handlebars.min.js?47228',
        'app/js/jquery.countdown.js?47228',
        'app/js/theme.js?47228',
        'app/js/global.js?47228',
        'app/js/custom.js?47228',
        'app/js/wow.min.js?47228',
        'app/js/jquery.lazy.min.js?47228',
        'app/vue/vue.js'

    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
