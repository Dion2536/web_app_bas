var csrfToken = $('meta[name="csrf-token"]').attr("content");

const store = new Vuex.Store({
    state: {
        dataCart: [],
        resultsData: []
    },
    mutations: {
        STATE_CART(state, item) {
            state.dataCart = item;
        },
        ADD_TO_CART(state,item,csrfToken){
            jQuery.post("/orders/add-cart",{item,_csrf: csrfToken},function(res){
                if(res.status==="ok"){
                    state.dataCart= res.data
                    Swal.fire({
                        type: "success",
                        title: "เพิ่มเข้าตะกร้าสินค้า",
                        showConfirmButton: false,
                        timer: 1500
                    })
                }else if(res.status ==='not-balance'){
                    Swal.fire({
                        type: "error",
                        title: "สินค้าหมด",
                        showConfirmButton: false,
                        timer: 1500
                    })
                }
            });
        },
        ADD_AMOUNT(state,item){
            jQuery.post("/orders/add-amount",{item,_csrf: csrfToken},function(res){
                console.log(res)
            });
        },
        REMOVE_CART(state, model){
            let dataArray = state.dataCart;
            console.log(model.items)
            jQuery.get("/orders/remove-order?id="+model.items.order_detail_id,function(res){
                // console.log(res)
            });
            dataArray.splice(model.index,1);
        },
    },
    actions: {
        checkCartOrder: (state) =>{
            axios.get("/orders/check-cart-order").then( (res) => {
                if(res.data.status ==="ok"){
                    let data =  res.data.data;
                    state.commit("STATE_CART",data)
                }
            })
        },
    },
    getters: {
        getCartCount(state){
            let total = 0;
            if(state.dataCart.length > 0){
                total =  state.dataCart.map(item=>item.amount).reduce((sum,amount) => {
                    return sum+amount;
                }, 0);
            }
            return total;
        },
        getDataCart: (state) => {
            return state.dataCart
        }
    }
});