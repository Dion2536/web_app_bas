<?php

namespace app\components;

use yii\base\Component;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use Yii;
class GlobalComponent extends Component
{
    public $content;
    public $title_name;
    public function init()
    {
        parent::init();
        $this->content = 'Hello Yii 2.0';

    }

    public function display($content = null)
    {
        if ($content != null) {
            $this->content = $content;
        }
        echo Html::encode($this->content);
    }

    public function breadcrumbDisplay($content = null){
        if ($content != null) {
            $this->title_name = $content;
        }
        echo '
            <section class="section-pagetop bg-gradient-orange  ">
        <div class="container">
            <h2 class="title-page">'.Html::encode($this->title_name).' </h2>
            <nav>
                <ol class="breadcrumb text-white">
                    <li class="breadcrumb-item "><a href="'.Yii::$app->homeUrl .'">หน้าหลัก</a></li>
                    <li class="breadcrumb-item active"
                        aria-current="page">'.Html::encode($this->title_name).' </li>
                </ol>
            </nav>
        </div>
    </section>
        ';
    }

    public function status($key_id)
    {
        $status = [
            0 => 'ไม่ใช้งาน',
            1 => 'ใช้งาน',
        ];
        if ($key_id != null) {
            return ArrayHelper::getValue($status,$key_id,[]);
        }
    }

    public function actionSendLine($message = null)
    {
        $sToken = Yii::$app->params['token_lineNotify'];
        $chOne = curl_init();
        curl_setopt($chOne, CURLOPT_URL, "https://notify-api.line.me/api/notify");
        curl_setopt($chOne, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($chOne, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($chOne, CURLOPT_POST, 1);
        curl_setopt($chOne, CURLOPT_POSTFIELDS, "message=" . $message);
        $headers = array('Content-type: application/x-www-form-urlencoded', 'Authorization: Bearer ' . $sToken . '',);
        curl_setopt($chOne, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($chOne, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($chOne);
        //Result error
        if (curl_error($chOne)) {
            echo 'error:' . curl_error($chOne);
        }

        curl_close($chOne);
    }
}

?>