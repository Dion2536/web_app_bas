<?php

use yii\helpers\Html;

$this->title = 'จ่ายสั่งทำสินค้า';
?>
<?= Yii::$app->global->breadcrumbDisplay($this->title) ?>
<div class="container" id="mete_order">
    <br>
<div class="row">
    <div class="col-md-5">
        <div class="card border-primary">
            <div class="card-body">
                <h3 class="card-text">รหัสสั่งทำ <?=@$model->id?></h3>
                <h5 class="card-title"><?=@$model->name?></h5>
                <p class="card-text"><?=@$model->details?></p>
                <p class="card-text">ระยะเวลา <?=@$model->time_length?></p>
                <p class="card-text">ราคามัดจำ <?=@number_format($model->deposit_price,0)?> บาท</p>
                <p class="card-text">ราคา <?=@number_format($model->price,0)?> บาท</p>
            </div>
        </div>
    </div>
    <div class="col-md-7" >
        <div class="card" v-if="!select_bank">
            <div class="card-body border-top">
                <h4>วิธีการชำระเงิน</h4>

                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#home">โอนธนาคาร</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#menu1">โอนพร้อมเพย์</a>
                    </li>

                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane container active" id="home">
                        <br>
                        <div class="row">
                            <div class="col-md-2">
                                <label>เลือกธนาคาร</label>
                            </div>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col" v-for="bk in bank">
                                        <input type="radio" :id="bk.id" :value="bk.id" v-model="bank_id">
                                        <label :for="bk.id">{{bk.bank_name}}</label>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <table class="table table-md table-striped table-inverse table-responsive  mb-4  "
                               width="100%">
                            <thead class="thead-inverse" v-for="model in findBank">
                            <tr>
                                <th>ธนาคาร</th>
                                <th colspan="2">{{model.bank_name}}</th>
                            </tr>
                            <tr>
                                <th>ชื่อบัญชื่</th>
                                <th colspan="2">{{model.name}}</th>
                            </tr>
                            <tr>
                                <th>เลขที่บัญชี</th>
                                <th colspan="2">{{model.number_bank}}</th>
                            </tr>
                            <tr>
                                <th>จำนวนเงินที่ต้องโอน</th>
                                <th><strong class="text-danger "><?=number_format($model->deposit_price,0)?></strong> บาท</th>
                            </tr>
                            </thead>
                        </table>
                        <br>
                        <div class="text-right">
                            <button class="btn btn-outline-success text-right" :disabled="bank_id==null"
                                    @click="handlePay"> ยืนยัน
                            </button>
                        </div>
                    </div>
                    <div class="tab-pane container fade" id="menu1">
                        <br>
                        <div class="row">
                            <div class="col-md-2">
                                <label>เลือก</label>
                            </div>
                            <div class="col-md-9">
                                <div class="row">
                                    <div class="col" v-for="bk in bankPay">
                                        <input type="radio" :id="bk.id" :value="bk.id" v-model="bank_id">
                                        <label :for="bk.id"> {{bk.name}} ( {{bk.number_bank}} ) </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div class="text-right">
                            <button class="btn btn-outline-success text-right" :disabled="bank_id==null"
                                    @click="handlePay"> ยืนยัน
                            </button>
                        </div>
                    </div>
                </div>
            </div> <!-- card-body.// -->
        </div>
        <div class="card  mb-4" v-if="select_bank">
            <div class="card-body">
                <div class="text-center">
                    <h6 class="text-primary">ระยะเวลาในการโอน 10 นาที</h6>
                    <h1 class="text-danger display-4">{{time}}</h1>
                </div>
                <hr>
                <label>จำนวนเงินมัดจำ</label>
                <input value="<?=$model->deposit_price?>" class="form-control" disabled>
                <hr>
                <div class="row">
                    <div class="col-6">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <button class="btn btn-outline-secondary btn-sm" type="button"><i
                                            class="fa fa-clock"></i></button>
                            </div>
                            <select class="custom-select" v-model="payment.hours">
                                <option selected>เลือกชั่วโมง...</option>
                                <option :value="hr.hours" v-for="hr in hoursArray">{{hr.hours}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <button class="btn btn-outline-secondary btn-sm" type="button"><i
                                            class="fa fa-clock"></i></button>
                            </div>
                            <select class="custom-select" :disabled="payment.hours==null"
                                    v-model="payment.time">
                                <option selected>เลือกนาที...</option>
                                <option :value="mt.time" v-for="mt in minutesArray">{{mt.time}}</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-12">
                                    <textarea class="form-control" rows="3" placeholder="หมายเหตุ"
                                              v-model="payment.remark">
                                    </textarea>
                    </div>
                </div>
                <hr>
                <button class="btn btn-success btn-block" @click="saveMateOrder" :disabled="check_btn(payment)">
                    ยืนยันการชำระ
                </button>
            </div> <!-- card-body.// -->
        </div> <!-- card.// -->
    </div>
</div>
</div>
<?php
$bank = \yii\helpers\Json::encode(\app\models\BankPayment::find()->where(['bank_type' => 1])->all());
$bankPay = \yii\helpers\Json::encode(\app\models\BankPayment::find()->where(['bank_type' => 2])->all());
$this->registerJs('
let app_cart_list = new Vue({
    el:"#mete_order",
    store,
    data:{
        bank_id:null,
        select_bank:false,
        payment:{
            hours:null,
            time:null,
            remark:null
        },
        date: moment(60 * 10 * 1000),
        bank:' . $bank . ',
        bankPay:' . $bankPay . ',
        minutes:[],
        hours:[]
    },
    mounted(){
       for(var h=1; h <=  24 ;h++){
        let jsonH = {}
        jsonH ={
            hours : h
        }
             this.hours.push(jsonH)
        }
      
        for(var i =1 ; i< 60 ;i++){
        let json ={
            time: i
        }
            this.minutes.push(json)
        }
    },
    computed:{
    minutesArray(){
        var d = new Date();
        var m = d.getMinutes();
        return this.minutes.filter(item=>item.time > m-3)
    },
     hoursArray(){
        var d = new Date();
        var h = d.getHours();
       return  this.hours.filter((item)=>{
         if(item.hours > h-2){
            if(item.hours ==24) { item.hours ="00"}
            return item
          }
         })
    },
     time: function(){
      return this.date.format(\'mm:ss\');
    },
    findBank(){
        return this.bank.filter(item=>item.id == this.bank_id);
    }
    
    },
    methods: {
       saveMateOrder(){
         let item ={
            payment:this.payment,
            bank_id:this.bank_id,
            mate_order_id:'.$model->id.'
        }
          return  jQuery.post("/mate-order/save-payment",{item,_csrf: csrfToken},function(res){
        console.log(res)
        if(res.data == "ok"){
        Swal.fire({
            type: "success",
            title: "ชำระมัดจำสำเร็จ !",
            showConfirmButton: false,
            timer: 5000
          })
         window.location.href="/mate-order/index";
        }
         })
       },
       check_btn(item){
            if(item.hours && item.time){
                return false;
            }else{
                return true;
            }
       },
       handlePay(){
     this.select_bank = true;
     let interval = setInterval(() => {
        let date  = moment(this.date.subtract(1, \'seconds\'))
         let time = date.format(\'mm:ss\');
          this.date = moment(this.date.subtract(0, \'seconds\'));
            if(time === "00:00"){
              this.payment_set = false;
              this.select_bank = false;
              this.bank_id = null;
              this.date = moment(60 * 10 * 1000),
              this.payment={
                hours:null,
                time:null,
                remark:null
             }
             window.location.href="/mate-order/index"
           clearInterval(interval); 
          }
       }, 1000);
    }
    }
    });
');
?>