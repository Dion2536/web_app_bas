<?php

use yii\helpers\Html;

$this->title = 'สั่งทำสินค้า';
?>
<?= Yii::$app->global->breadcrumbDisplay($this->title) ?>
<br>
<div class="row">
    <div class="col-md-9">
        <ul class="nav nav-tabs">
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#index">สั่งทำสินค้า</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#history">ประวัติสั่งทำ</a>
            </li>

        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane  active" id="index">
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-title">รายการสั่งทำ <h4> รหัสสมาชิกของท่าน <strong
                                        class="text-danger"><?= @\app\models\Profile::findOne(['user_id' => Yii::$app->user->id])->member_code ?></strong>
                            </h4></h2>
                        <div class="alert alert-warning alert-dismissible fade show" role="alert">
                            <strong> <i class="fa fa-info-circle"></i> จะสั่งทำสินค้า กรุณา</strong> <span
                                    class="text-success"> @ไลน์ </span><span
                                    class="text-danger">หรือโทรสอบถาม</span> และแจ้งรหัสสมาชิกเพิ่มทำรายการ...
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="col-md-9">

                            <?php
                            $user_id = Yii::$app->user->id;
                            foreach (\app\models\MateOrder::find()->where("status!=6 and user_id = $user_id")->orderBy([
                                'status' => SORT_ASC,
                                'id' => SORT_DESC

                            ])->all() as $mo):?>
                                <div class="card text-left">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-7">
                                                <p>ชื่อรายการ</p>
                                                <h4 class="card-title"><?= @$mo->name ?></h4>
                                                <p class="card-text"><?= @$mo->details ?></p>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="pull-right text-right">
                                                    <h4 class="text-secondary">
                                                        ราคาเต็ม <?= number_format($mo->price, 0) ?>
                                                        บาท</h4>
                                                    <p class="text-secondary">
                                                        ราคามัดจำ <?= number_format($mo->deposit_price, 0) ?>
                                                        บาท</p>
                                                    <?php if ($mo->status == 1): ?>
                                                        <a href="<?= \yii\helpers\Url::to(['mate-order/view', 'id' => $mo->id]) ?>"
                                                           class="btn btn-outline-warning">โอนมัดจำ <strong
                                                                    class="text-danger">(<?= @number_format($mo->deposit_price, 0) ?>
                                                                บาท)</strong></a>
                                                    <?php elseif ($mo->status != 1): ?>
                                                        <button class="btn btn-outline-success">โอนมัดจำแล้ว
                                                            <?php if ($mo->status == 2): ?>
                                                                <strong class="text-warning">(รอตรวจสอบ...)</strong>
                                                            <?php elseif ($mo->status == 3): ?>
                                                                <strong class="text-danger">(ตรวจสอบแล้ว...)</strong>
                                                            <?php elseif ($mo->status == 4): ?>
                                                                <strong class="text-danger">(กำลังดำเนินการ...)</strong>
                                                            <?php elseif ($mo->status == 5): ?>
                                                                <strong class="text-danger">(กำลังจัดส่ง...)</strong>
                                                            <?php elseif ($mo->status == 6): ?>
                                                                <strong class="text-danger">(จัดส่งสำเร็จ)</strong>
                                                            <?php endif; ?>
                                                        </button>
                                                    <?php endif; ?>
                                                    <p> ระยะเวลา <?= @$mo->time_length ?></p>
                                                </div>
                                            </div>
                                        </div>


                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane  fade" id="history">
                <div class="card">
                    <div class="card-body">
                        <h2 class="card-title">ประวัติสั่งทำ <h4> รหัสสมาชิกของท่าน <strong
                                        class="text-danger"><?= @\app\models\Profile::findOne(['user_id' => Yii::$app->user->id])->member_code ?></strong>
                            </h4></h2>
                        <div class="col-md-9">
                            <?php
                            $user_id = Yii::$app->user->id;
                            foreach (\app\models\MateOrder::find()->where("status=6 and user_id = $user_id")->orderBy([
                                'status' => SORT_ASC,
                                'id' => SORT_DESC
                            ])->all() as $mo):?>
                                <div class="card text-left">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-md-7">
                                                <p>ชื่อรายการ</p>
                                                <h4 class="card-title"><?= @$mo->name ?></h4>
                                                <p class="card-text"><?= @$mo->details ?></p>
                                            </div>
                                            <div class="col-md-5">
                                                <div class="pull-right text-right">
                                                    <h4 class="text-secondary">
                                                        ราคาเต็ม <?= number_format($mo->price, 0) ?>
                                                        บาท</h4>
                                                    <p class="text-secondary">
                                                        ราคามัดจำ <?= number_format($mo->deposit_price, 0) ?>
                                                        บาท</p>
                                                    <?php if ($mo->status == 1): ?>
                                                        <a href="<?= \yii\helpers\Url::to(['mate-order/view', 'id' => $mo->id]) ?>"
                                                           class="btn btn-outline-warning">โอนมัดจำ <strong
                                                                    class="text-danger">(<?= @number_format($mo->deposit_price, 0) ?>
                                                                บาท)</strong></a>
                                                    <?php elseif ($mo->status != 1): ?>
                                                        <button class="btn btn-success">โอนมัดจำแล้ว

                                                            <?php if ($mo->status == 6): ?>
                                                                <strong class="text-danger">(จัดส่งสำเร็จ...)</strong>
                                                            <?php endif; ?>
                                                        </button>
                                                    <?php endif; ?>
                                                    <p> ระยะเวลา <?= @$mo->time_length ?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3 ">
        <h4 class=""> ติดต่อสอบถามสั่งทำและราคา</h4>
        <div class="card" style="width: 18rem;">
            <img class="card-img-top" src="<?= Yii::getAlias('@web/app_frontend/images/line/10241.jpg') ?>"
                 alt="Card image cap">
            <div class="card-body">
                <a href="https://line.me/ti/p/Q_4rGTsu8j" class="btn btn-outline-success btn-block">@สอบถามและพูดคุย</a>
                <a href="084-5585-482" class="btn btn-outline-warning btn-block"><i class="fa fa-phone"></i>
                    084-5585-482</a>
            </div>
        </div>
    </div>
</div>