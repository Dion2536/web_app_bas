<?php

/* @var $this \yii\web\View */
/* @var $content string */
$title = '';

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use app\assets\AppFrontendAsset;

AppFrontendAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="bg-white shadow-sm">
<?php $this->beginBody() ?>

<header class="section-header">
    <section class="header-top-light border-bottom">
        <div class="container">

            <nav class="d-flex align-items-center flex-column flex-md-row">

                <ul class="nav mr-md-auto">

                </ul>

                <?php if (Yii::$app->user->isGuest): ?>

                    <ul class="nav ">

                        <li class="nav-item"><a href="<?= Url::to(['site/login']) ?>" class="nav-link"> <i
                                        class="fa fa-user-circle"></i> เข้าสู่ระบบ </a></li>
                    </ul>

                <?php else: ?>
                    <ul class="nav">
                        <li class="nav-item"><a href="<?= Url::to(['site/profile', 'show' => 'sub']) ?>"
                                                class="nav-link"> บัญชีของฉัน </a></li>
                        <li class="nav-item"><a href="#" class="nav-link"> การซื้อของฉัน </a></li>
                    </ul>
                    <div class="widgets-wrap float-md-right mt-2">
                        <div class="icontext">
                            <div class="text">

                            </div>
                        </div>
                        <div class="icontext">
                            <a href="<?= Url::to(['site/profile', 'show' => 'profile']) ?>"
                               class="icon icon-sm rounded-circle border"><i class="fa fa-user"></i></a>
                            <div class="text">
                                <span class="text-black">สวัสดี (<?= Yii::$app->user->identity->username ?>)
                                <br>
                                    รหัสสมาชิก <?= @\app\models\Profile::findOne(['user_id' => Yii::$app->user->id])->member_code ?>
                                </span>
                                <div>

                                    <?php
                                    echo
                                        Html::beginForm(['/site/logout'], 'post')
                                        . Html::submitButton(
                                            'ออกจากระบบ',
                                            ['class' => 'btn btn-link ']
                                        )
                                        . Html::endForm();
                                    ?>
                                </div>
                            </div>
                        </div>


                    </div>
                <?php endif; ?>

            </nav> <!-- nav .// -->
        </div> <!-- container //  -->
    </section> <!-- header-top .// -->
    <section class="border-bottom">
        <nav class="navbar navbar-main  navbar-expand-lg navbar-light">
            <div class="container">
                <a class="navbar-brand" href="<?= Yii::$app->homeUrl ?>"><?= Yii::$app->name ?></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main_nav"
                        aria-controls="main_nav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="main_nav">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="<?= Yii::$app->homeUrl ?>">สินค้าทั้งหมด </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?= Url::to(['mate-order/index']) ?>">สั่งทำสินค้า</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="<?= Url::to(['site/about']) ?>">เกี่ยวกับเรา</a>
                        </li>


<!--                        <li class="nav-item dropdown">-->
<!--                            <a class="nav-link dropdown-toggle" data-toggle="dropdown"-->
<!--                               href="http://example.com">More</a>-->
<!--                            <div class="dropdown-menu" aria-labelledby="dropdown07">-->
<!--                                <a class="dropdown-item" href="#">Foods and Drink</a>-->
<!--                                <a class="dropdown-item" href="#">Home interior</a>-->
<!--                                <div class="dropdown-divider"></div>-->
<!--                                <a class="dropdown-item" href="#">Category 1</a>-->
<!---->
<!--                            </div>-->
<!--                        </li>-->
                    </ul>
                    <form class="form-inline my-2 my-lg-0" action="/site/index">
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="ค้นหาสินค้า..">
                            <div class="input-group-append">
                                <button class="btn btn-primary" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                    <div class="widget-header  ml-5 " id="basket" v-cloak>
                        <a href="<?= Url::to(['site/list-cart']) ?>" class="icon icon-sm rounded-circle border"><i
                                    class="fa fa-shopping-cart"></i></a>
                        <span class="badge badge-pill badge-danger notify">{{dataCount}}</span>
                    </div>
                </div> <!-- collapse .// -->
            </div> <!-- container .// -->
        </nav>
    </section> <!-- header-main .// -->
</header>


<?php
//        NavBar::begin([
//            'brandLabel' => Yii::$app->name,
//            'brandUrl' => Yii::$app->homeUrl,
//            'options' => [
//                'class' => 'navbar-inverse navbar-fixed-top',
//            ],
//        ]);
//     echo Nav::widget([
//            'options' => ['class' => 'navbar-nav navbar-right'],
//            'items' => [
//                ['label' => 'Home', 'url' => ['/site/index']],
//                ['label' => 'About', 'url' => ['/site/about']],
//                ['label' => 'Contact', 'url' => ['/site/contact']],
//                Yii::$app->user->isGuest ? (
//                    ['label' => 'Login', 'url' => ['/site/login']]
//                ) : (
//                    '<li>'
//                    . Html::beginForm(['/site/logout'], 'post')
//                    . Html::submitButton(
//                        'Logout (' . Yii::$app->user->identity->username . ')',
//                        ['class' => 'btn btn-link logout']
//                    )
//                    . Html::endForm()
//                    . '</li>'
//                )
//            ],
//        ]);
//        NavBar::end();
?>

<div class="">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],

    ]) ?>
    <?= Alert::widget() ?>
    <?= $content ?>
</div>

<?php
$JS = '
let app_basket = new Vue({
    el:"#basket",
    store,
    data:{
        title:"ตะกร้าOrder",
        showSuccess:false
    },
    mounted(){
        this.$store.dispatch("checkCartOrder")
    },
    computed:{
        dataCount(){
            let vm  = this;
            return vm.$store.getters.getCartCount
        },
        dataResult(){
            let vm  = this;
            return vm.$store.getters.getDataCart
        }
    },
     
    })
    ';
$this->registerJs($JS);
?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
