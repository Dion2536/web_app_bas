<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php
$payment= \app\models\Payment::findOne(['order_id'=>$order->id]);
$orderJson = \yii\helpers\Json::encode($order);
$orderTotal =Yii::$app->db->createCommand("SELECT sum(p.price * od.amount) from order_details od
left join product p on od.product_id = p.id
WHERE order_id = $order->id")->queryOne();
?>
<link rel="stylesheet" href="<?= Yii::getAlias('@web/app_frontend/css/bootstrap.css')?> ">
<link rel="stylesheet" href="<?= Yii::getAlias('@web/app_frontend/css/sweetalert2.min.css')?> ">

<script src="https://static.line-scdn.net/liff/edge/2.1/sdk.js"></script>
<script src="<?= Yii::getAlias('@web/app_frontend/vue/axios.min.js')?>" ></script>
<script src="<?= Yii::getAlias('@web/app_frontend/vue/vue.js')?>" ></script>
<script src="<?= Yii::getAlias('@web/app_frontend/vue/sweetalert2.min.js')?>" ></script>
<style>
    .shop{
        font-size: 2em;
    }
    .shop-led{
        font-size: 2em;
    }
    .shop-head{
          font-size: 1.5em;
      }

</style>

<div class="row" id="check" >
    <div class="col-sm">
        <div class="card border-dark mb-3 text-center"  >
            <div class="card-header shop-head">ยืนยันการชำระ #รหัสสินค้า <?=@$order->id?></div>
            <div class="card-body text-dark">
                <?php if($order->status ==1):?>
                <h1 class="card-title shop-led" > ราคารายการ <span class="text-danger"><?=@$orderTotal['sum']?></span> บ.</h1>

                <h1 class="shop">ช่วงเวลาชำระ : <span class="text-success"><?=@$payment->check_time?></span> น.</h1>
            <div class="">
                <button type="button" class="btn btn-outline-success btn-lg"  @click="check_ok"><h1>ยืนยัน</h1></button>
                <button type="button" class="btn  btn-lg btn-outline-danger" @click="check_not"><h1>ไม่ถูกต้อง</h1></button>
            </div>

                <?php elseif($order->status ==4):?>
                    <div class="text-center">
                        <h1 class="text-danger">ยกเลิกรายการนี้แล้ว !</h1>
                    </div>
                <?php else:?>
                <div class="text-center">
                    <h1>ทำรายการนี้แล้ว !</h1>
                </div>
                <?php endif;?>
            </div>
        </div>
    </div>
</div>
<script>
    document.addEventListener('DOMContentLoaded', (event) => {

    })
    function btn_close() {
        window.close();
    }
    var app = new Vue({
        el: '#check',
        data: {
            dataJson :  null,
        },
        mounted() {

        },
        methods:{
            check_not:function(){
                axios.get('/orders/not-confirm?id='+<?=@$order->id?>).then(function (response) {
                    if(response.data =='ok'){
                        Swal.fire({
                            type: "error",
                            title: "ยกเลิกสำเร็จ !",
                            showConfirmButton: false,
                            timer: 2000
                        });
                        setTimeout(function(){
                            location.reload();
                            window.close();
                        }, 2000);
                    }
                })
            },
            check_ok:function(){
                   //window.close();
                axios.get('/orders/confirm?id='+<?=@$order->id?>).then(function (response) {
                    if(response.data =='ok'){
                       Swal.fire({
                            type: "success",
                            title: "ทำรายการสำเร็จ !",
                            showConfirmButton: false,
                            timer: 2000
                        });
                        setTimeout(function(){
                            location.reload();
                            window.close();
                        }, 2000);
                    }
                });
            }
        }
    })

</script>