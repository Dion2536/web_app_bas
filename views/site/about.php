<?php
use yii\helpers\Html;
$this->title = 'เกี่ยวกับเรา';
?>
<div class="site-about">
    <?=Yii::$app->global->breadcrumbDisplay($this->title)?>

    <div class="container">
        <article class="card">
            <div class="card-body">
                <div class="row">
                    <aside class="col-md-6">
                        <h3>ร้าน<?=Yii::$app->name?></h3>
                        <dl class="row">
                            <dt class="col-sm-3">รับงานปั้นลอยตัว</dt>
                            <dd class="col-sm-9">13.3-inch LED-backlit display with IPS</dd>

                            <dt class="col-sm-3">งานปั้นโนนสูง</dt>
                            <dd class="col-sm-9">2.3GHz dual-core Intel Core i5</dd>

                            <dt class="col-sm-3">งานออกแบบปั้นต่างๆ</dt>
                            <dd class="col-sm-9">720p FaceTime HD camera</dd>

                            <dt class="col-sm-3">รับออกแบบตัวอักษร</dt>
                            <dd class="col-sm-9">8 GB RAM or 16 GB RAM</dd>

                            <dt class="col-sm-3">Graphics</dt>
                            <dd class="col-sm-9">Intel Iris Plus Graphics 640</dd>
                        </dl>
                    </aside>
                    <aside class="col-md-6">
                        <h3>ที่อยู่</h3>
                        <ul class="list-check">
                            <li>Best performance of battery</li>
                            <li>5 years warranty for this product</li>
                            <li>Amazing features and high quality</li>
                            <li>Best performance of battery</li>
                            <li>5 years warranty for this product</li>
                            <li>Amazing features and high quality</li>
                        </ul>
                    </aside>
                </div> <!-- row.// -->
                <hr>
                <h4>
                    ช่องทางติดต่อ
                </h4>
                <div class="row">
                    <div class="col-md-4">
                        <div class="card" style="width:20rem;margin:20px 0 24px 0; height: 450px">
                            <img class="card-img-top text-center"  src="<?=Yii::getAlias('@web/app_frontend/images/line/call.jpeg')?>" alt="image" style="width:250px">
                            <div class="card-body text-center">
                                <h4 class="card-title text-success"></h4>
                                <a href="084-5486-806" class="btn btn-outline-success btn-block" ><i class="fa fa-phone"></i> 084-5486-806</a>
                                <a href="084-5486-806" class="btn btn-outline-success btn-block" > <i class="fa fa-phone"></i> 084-5486-806</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card" style="width:20rem;margin:20px 0 24px 0;height: 450px">
                            <img class="card-img-top" src="<?=Yii::getAlias('@web/app_frontend/images/line/10241.jpg')?>" alt="image" style="width:width:290px">
                            <div class="card-body text-center">
                                <h4 class="card-title text-success"></h4>
                                <a href="https://line.me/ti/p/Q_4rGTsu8j" class="btn btn-outline-primary btn-block">@เพิ่มเพื่อนเพิ่มคุยกับเรา</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- card-body.// -->
        </article>
    </div>
</div>
