<?php
$this->title = 'ติดตามสินค้า';
$this->registerCssFile("@web/app_frontend/css/site.css", [
    'depends' => [\app\assets\AppFrontendAsset::className()],
], 'css');
?>
<?php
$user_id = Yii::$app->user->id;
    $order = \app\models\Order::find()->where(['created_by' => $user_id,'status'=>[1,2]])->orderBy([
    'status' => SORT_ASC,

    ])->all();
$id = Yii::$app->request->get('id');

?>
<?php if(isset($id)):?>
        <output>
            <div class="card">
                <article class="card-body">
                    <header class="mb-4">
                        <h4 class="card-title">รายการสินค้า</h4>
                    </header>
                    <div class="row">
                        <?php foreach (\app\models\OrderDetails::findAll(['order_id'=>$id]) as $od):?>
                        <div class="col-md-6">
                            <figure class="itemside  mb-3">
                                <div class="aside"><img src="bootstrap-ecommerce-html/images/items/1.jpg" class="border img-xs"></div>
                                <figcaption class="info">
                                    <p><?php $p =\app\models\Product::findOne(['id'=>$od->product_id]); echo $p->name; ?> </p>
                                    <span><?=@$od->amount?> x <?=@number_format($p->price,0)?> = รวม: <?=@number_format($od->amount*$p->price,0)?></span>
                                </figcaption>
                            </figure>
                        </div> <!-- col.// -->
                <?php endforeach;?>
                    </div> <!-- row.// -->
                </article> <!-- card-body.// -->
                <article class="card-body border-top">
                    <dl class="row">
                        <dt class="col-sm-10">Subtotal: <span class="float-right text-muted">2 items</span></dt>
                        <dd class="col-sm-2 text-right"><strong>$1,568</strong></dd>
                        <dt class="col-sm-10">Discount: <span class="float-right text-muted">10% offer</span></dt>
                        <dd class="col-sm-2 text-danger text-right"><strong>$29</strong></dd>
                        <dt class="col-sm-10">Delivery charge: <span class="float-right text-muted">Express delivery</span></dt>
                        <dd class="col-sm-2 text-right"><strong>$120</strong></dd>
                        <dt class="col-sm-10">Tax: <span class="float-right text-muted">5%</span></dt>
                        <dd class="col-sm-2 text-right text-success"><strong>$7</strong></dd>
                        <dt class="col-sm-10">Total:</dt>
                        <dd class="col-sm-2 text-right"><strong class="h5 text-dark">$1,650</strong></dd>
                    </dl>
                </article> <!-- card-body.// -->
            </div>
        </output>
<?php else:?>
<?php if(isset($order)):?>
<?php foreach ($order as $model): ?>
    <div class="card">
        <div class="card-body">
            <h5>#Order : <?= $model->id ?> </h5>
            <div class="stepwizard">
                <div class="stepwizard-row">
                    <div class="stepwizard-step">
                        <?php if ($model->status == 1 || 2): ?>
                            <button type="button" class="btn btn-success btn-circle">
                                <i class="fa fa-check"></i>
                            </button>
                        <?php else: ?>
                            <button type="button" class="btn btn-dark btn-circle"><i class="fa fa-shopping-cart"></i>
                            </button>
                        <?php endif; ?>
                        <p>สั่งซื้อสินค้า</p>
                        <p>
                            <small> <?=@ \app\models\OrderDetails::findOne(['order_id' => $model->id])->created_at ?></small>
                        </p>
                    </div>
                    <div class="stepwizard-step">
                        <?php if ($model->status == 1 ): ?>
                            <button type="button" class="btn btn-warning btn-circle">
                                <i class="fa fa-circle-o-notch fa-spin fa-fw"></i>
                            </button>
                         <?php elseif ($model->status == 2 ): ?>
                            <button type="button" class="btn btn-success btn-circle">
                                <i class="fa fa-check"></i>
                            </button>
                        <?php else: ?>
                            <button type="button" class="btn btn-dark btn-circle"><i class="fa fa-bank"></i></button>
                        <?php endif; ?>

                        <p>ชำระสินค้า
                            <?php if ($model->status == 1): ?>
                                <span class="text-warning" style="font-size: 12px;">(<i class="fa fa-edit"></i> รอตรวจสอบ)</span>
                            <?php else: ?>
                                <span class="text-success" style="font-size: 12px;">(<i class="fa fa-check"></i> ตรวจสอบแล้ว)</span>
                            <?php endif; ?>
                        </p>
                        <p><small> <?=@ \app\models\Payment::findOne(['order_id' => $model->id])->created_at ?></small>
                        </p>

                    </div>
                    <div class="stepwizard-step">
                        <?php
                        $ship = \app\models\Shipping::findOne(['order_id'=>$model->id]);
                        if ($ship&&$ship->status == 1): ?>
                            <button type="button" class="btn btn-warning btn-circle">
                                <i class="fa fa-refresh fa-spin fa-fw"></i>
                            </button>
                            <p>จัดส่ง
                                <span class="text-warning" style="font-size: 12px;">( กำลังรอจัดส่ง ...)</span>
                            </p>
                           <?php elseif ($ship&&$ship->status == 2): ?>
                            <button type="button" class="btn btn-info btn-circle">
                                <i class="fa fa-recycle fa-spin fa-fw"></i>
                            </button>
                            <p>จัดส่ง
                                <span class="text-success" style="font-size: 12px;">( กำลังจัดส่ง ...)</span><br>
                                <span class="text-success" style="font-size: 12px;">(ระยะเวลา <span class="text-danger"><?=$ship->date_send ?></span> ) </span><br>
                                <span class="text-success" style="font-size: 12px;">(ประมาณวัน
                                <span class="text-danger">
                                    <?php
                                    $date1 =  date_create(substr($ship->date_send,0,10));
                                    $date2 =  date_create(substr($ship->date_send,13,10));
                                    $interval = date_diff($date1, $date2);
                                    echo $interval->format('%a ') . "วัน";?> )
                                </span>
                                </span><br>

                            </p>
                        <?php elseif ($ship&&$ship->status == 3): ?>
                            <button type="button" class="btn btn-success btn-circle">
                                <i class="fa fa-check"></i>
                            </button>
                            <p>จัดส่ง
                                <span class="text-success" style="font-size: 12px;">( ส่งสำเร็จ ...)</span>
                            </p>
                        <?php else: ?>
                            <button type="button" class="btn btn-dark btn-circle" disabled="disabled"><i
                                        class="fa fa-truck"></i></button>
                            <p>จัดส่ง
                            </p>

                        <?php endif; ?>

                    </div>
                    <div class="stepwizard-step">


                        <button type="button" class="btn btn-dark btn-circle" disabled="disabled"><i
                                    class="fa fa-handshake"></i></button>

                        <p>รับสินค้าแล้ว</p>
                    </div>
                </div>
            </div>
            <div class="text-right">
                <small> <a href="<?=\yii\helpers\Url::to(['/site/profile','show'=>'sub','id'=>$model->id])?>">รายละเอียดเพิ่ม >></a></small>
            </div>

        </div>
    </div>
    <br>
<?php endforeach; ?>
    <?php else:?>
    <div class="text-center">
        <a href="<?=\yii\helpers\Url::to(['site/index'])?>" class="text-secondary">ส่ังซื้อสินค้าเพิ่มเติม !</a>
    </div>
<?php endif;?>

<?php endif;?>
