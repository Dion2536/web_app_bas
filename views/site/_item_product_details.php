<?php
$this->title = 'รายละเอียดสินค้า';
$id = Yii::$app->request->get('id');
if($id){
    $product =  \app\models\Product::findOne(['id'=>$id]);
    $product->view = $product->view+1;
    $product->save();
}
?>

    <section class="section-pagetop bg-gradient-orange  ">
        <div class="container">
            <h2 class="title-page"><?= \yii\helpers\Html::encode($this->title) ?></h2>
            <nav>
                <ol class="breadcrumb text-white">
                    <li class="breadcrumb-item "><a href="<?= Yii::$app->homeUrl ?>">หน้าหลัก</a></li>
                    <li class="breadcrumb-item active"
                        aria-current="page"><?= \yii\helpers\Html::encode($this->title) ?></li>
                </ol>
            </nav>
        </div>
    </section>
    <div class="container" id="app-details" v-cloak>
        <br>

        <div class="card" v-for="model in product_details">
            <div class="row no-gutters">
                <aside class="col-sm-5 border-right">
                    <article class="gallery-wrap">
                        <div class="img-big-wrap">
                            <div><a :href="model.photos" data-fancybox=""><img :src="model.photos"></a></div>
                        </div> <!-- slider-product.// -->
                        <div class="img-small-wrap">
                            <div class="item-gallery" v-for="ph in photos_list">
                                <a :href="<?= Yii::getAlias('@web') . '/uploads/' ?>+ph.name" data-fancybox="">
                                    <img :src="<?= Yii::getAlias('@web') . '/uploads/' ?>+ph.name">
                                </a>
                            </div>
                        </div> <!-- slider-nav.// -->
                    </article> <!-- gallery-wrap .end// -->
                </aside>
                <aside class="col-sm-7">
                    <article class="p-5">
                        <h3 class="title mb-3">{{model.name}}</h3>

                        <div class="mb-3">
                            <var class="price h3 text-warning">
                                <span class="currency"></span><span class="num">{{model.price}}</span>
                            </var>
                            <span>/บาท</span>
                        </div> <!-- price-detail-wrap .// -->


                        <div class="rating-wrap">
                            <div class="label-rating" v-if="model.view"> จำนวนดูสินค้า {{model.view}}</div>
                            <div class="label-rating">
                                <small class="label-rating text-success" v-if="model.amount">
                                    <i class="fa fa-clipboard-check"></i> จำนวน {{model.amount}} รายการ </small>

                                <span class="text-danger" v-else> สินค้าหมด </span>
                            </div>
                        </div> <!-- rating-wrap.// -->
                        <hr>
                        <div class="form-row">
                            <div class="form-group col-md flex-grow-0">
                                <div class="input-group input-spinner">
                                    <div class="input-group-prepend">
                                        <button class="btn btn-light" type="button" id="button-plus"
                                                @click="plusAmount(model)"> +
                                        </button>
                                    </div>
                                    <input type="text" class="form-control" v-model="amount_product">
                                    <div class="input-group-append">
                                        <button class="btn btn-light" type="button" id="button-minus"
                                                :disabled="amount_product==1" @click="amount_product--"> −
                                        </button>
                                    </div>
                                </div>  <!-- input-spinner.// -->
                            </div> <!-- col.// -->
                            <div class="form-group col-md">
                                <a href="#" class="btn  btn-outline-primary" v-if="model.amount" @click="addCart(model)"> <i
                                            class="fas fa-shopping-cart"></i> เพิ่มไปยังรถเข็น </a>
                                <a href="#" class="btn  btn-outline-success" @click="addCartPay(model)" v-if="model.amount"> <i
                                            class="fas fa-shopping-bag" ></i> ซื้อสินค้า </a>
                                <a href="<?=Yii::$app->homeUrl?>" class="btn  btn-outline-danger" v-else> <i class="fas fa-ban"></i>
                                    สินค้าหมด</a>
                            </div> <!-- col.// -->
                        </div> <!-- row.// -->

                        <hr>
                        <a href="#" class="title mt-2 h5">รายละเอียดสินค้า</a>
                        <dl style="width: ">
                            <dt></dt>
                            <dd><p v-html="model.details"></p></dd>
                        </dl>
                    </article> <!-- card-body.// -->
                </aside> <!-- col.// -->
            </div> <!-- row.// -->
        </div> <!-- card.// -->

        <hr class="divider">

        <div class="card card-body">
            <h5>สินค้าคล้ายกัน</h5>
            <div class="row">
                <div class="col-md-3" v-for="model in product_list">
                    <figure class="itemside mb-4"  >
                        <div class="aside"><img :src="model.photos" class="img-sm"></div>
                        <figcaption class="info align-self-center info-wrap">
                            <p class="title" style=" /* or inline-block */
  text-overflow: ellipsis;
  word-wrap: break-word;
  overflow: hidden;
  max-height: 3.6em;
  line-height: 1.8em;">{{model.name}}</p>
                            <a href="#"  class="btn btn-outline-success btn-sm" @click="itemProductDetails(model.id)"> ซื้อสินค้า
                                <i class="fa fa-shopping-cart"></i>
                            </a>

                        </figcaption>
                    </figure>
                </div> <!-- col.// -->

            </div> <!-- row.// -->
        </div>
    </div>


<?php
$this->registerJs('
var csrfToken = $(\'meta[name = "csrf-token"]\').attr("content");
var app = new Vue({
    store,
  el: "#app-details",
  data: {
      product_details : '.$model.',
      photos_list :'.$imagesList.',
      amount_product:1,
      product_list:'.$product_list.',
  },
  computed: {
  
  },
   methods: {
      plusAmount(model){
        if(this.amount_product < model.amount ){
            this.amount_product ++;
        }
      },
      itemProductDetails(id){
           window.location.href="/site/items-details?id="+id;
      },
      addCart:function(item) {
      item["amount_set"] = this.amount_product;
          this.$store.commit("ADD_TO_CART", item);
      },
      addCartPay:function(item) {
      item["amount_set"] = this.amount_product;
          this.$store.commit("ADD_TO_CART", item);
            window.location.href="/site/list-cart"
      }
   }
     
})
'
);

?>