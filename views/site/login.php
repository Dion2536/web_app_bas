<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'เข้าสู่ระบบ';
//$this->params['breadcrumbs'][] = $this->title;
$user = \app\models\User::findOne(['username'=>$model->username]);
?>
<?=Yii::$app->global->breadcrumbDisplay($this->title)?>
<div class="site-login" style="margin-top: 15px"  >
    <?php if($user):?>
        <?php if ($user->status ==9):?>

            <div class="alert alert-warning" role="alert">
                กรุณายืนยันข้อมูลผ่านอีเมล์ก่อน <span class="alert-link">เข้าสู่ระบบ</span>...
            </div>
        <?php endif;?>
    <?php endif;?>

    <?php if(Yii::$app->request->get('id')=='confirm'):?>
         <div class="alert alert-danger" role="alert">
                กรุณายืนยันข้อมูลผ่านอีเมล์ก่อน <span class="alert-link">เข้าสู่ระบบ</span>...
            </div>
    <?php endif;?>
<div class="row">

    <div class="col-sm-4 offset-4">
            <div class="card">
                <article class="card-body">
                    <a href="<?=\yii\helpers\Url::to(['site/signup'])?>" class="float-right btn btn-outline-success">สมัครสมาชิก</a>
                    <h4 class="card-title mb-4 mt-1"><?=\yii\helpers\Html::encode($this->title)?></h4>
                    <?php $form = ActiveForm::begin([
                        'id' => 'login-form',

                    ]); ?>
                        <div class="form-group">
                            <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
                        </div> <!-- form-group// -->
                        <div class="form-group">
                            <?= $form->field($model, 'password')->passwordInput() ?>
                        </div> <!-- form-group// -->
                        <div class="form-group">
                            <?= $form->field($model, 'rememberMe')->checkbox() ?>
                        </div> <!-- form-group// -->
                        <div class="form-group">
                            <?= Html::submitButton('เข้าสู่ระบบ', ['class' => 'btn btn-outline-primary btn-block', 'name' => 'login-button']) ?>
                        </div> <!-- form-group// -->
                    <?php ActiveForm::end(); ?>

                </article>
            </div> <!-- card.// -->
    </div>
</div>






</div>
