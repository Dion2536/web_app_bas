<?php
$this->title = 'รายการสั่งซื้อสินค้า';


?>
<?= Yii::$app->global->breadcrumbDisplay($this->title) ?>

    <div class="container" id="cart_list" v-cloak>
        <br>
        <div v-if="dataResult.length > 0">
            <div class="row" v-if="!payment_set">
                <aside class="col-lg-8">
                    <div class="card">
                        <div class="table-responsive">

                            <table class="table table-borderless table-shopping-cart">
                                <thead class="text-muted">
                                <tr class="small text-uppercase">
                                    <th scope="col">ชื่อสินค้า</th>
                                    <th scope="col" width="120">จำนวน</th>
                                    <th scope="col" width="120">ราคา</th>
                                    <th scope="col" class="text-right d-none d-md-block" width="200"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr v-for="(model,index) in dataResult">
                                    <td>
                                        <figure class="itemside align-items-center">
                                            {{index+1}}
                                            <div class="aside"><img
                                                        :src="<?= Yii::getAlias('@web/uploads/') ?>+model.photos"
                                                        class="img-sm"></div>
                                            <figcaption class="info">
                                                <a href="#" class="title text-dark" style=" width:200px;
  overflow:hidden;
  height:1.5em;">{{model.name}}</a>
                                            </figcaption>
                                        </figure>
                                    </td>
                                    <td>
                                        <div class="input-group input-spinner">
                                            <div class="input-group-prepend">
                                                <button class="btn btn-light" type="button" id="button-plus"
                                                        @click="addAmount(model)"> +
                                                </button>
                                            </div>
                                            <input type="text" class="form-control" v-model="model.amount" maxlength="3"
                                                   @change="inputAmount(model)" @keypress="isNumber($event)">
                                            <div class="input-group-append">
                                                <button class="btn btn-light" type="button" id="button-minus"
                                                        @click="minAmount(model)"> −
                                                </button>
                                            </div>
                                        </div>  <!-- input-spinner.// -->

                                    </td>
                                    <td>
                                        <div class="price-wrap">
                                            <var class="price">{{model.amount*model.price | formatNumber}}</var>
                                            <!--                                    <small class="text-muted"> $315.20 each </small>-->
                                        </div> <!-- price-wrap .// -->
                                    </td>
                                    <td class="text-right d-none d-md-block">
                                        <a href="#" class="btn btn-light" @click="removeCart(model,index)"> ลบ</a>
                                    </td>
                                </tr>

                                </tbody>
                            </table>

                        </div> <!-- table-responsive.// -->

                        <div class="card-body border-top">
                            <p class="icontext"><i class="icon text-success fa fa-truck"></i>ฟรีการจัดส่ง 1 - 2 วัน</p>
                        </div> <!-- card-body.// -->

                    </div> <!-- card.// -->
                </aside> <!-- col.// -->
                <aside class="col-lg-4">

                    <div class="card mb-3">
                        <div class="card-body">
                            <h6>ที่อยู่จัดส่ง</h6>
                            <div class="text-right"><a
                                        href="<?= \yii\helpers\Url::to(['/site/profile?show=address']) ?>"
                                        class="btn btn-sm btn-outline-dark">ที่อยู่เพิ่มเติม</a></div>
                            <?php if (isset($address)): ?>
                                <table class="table table-striped table-responsive" style="font-size: 14px;">
                                    <tbody>
                                    <tr>
                                        <th width="70px">ชื่อผู้รับ</th>
                                        <td><?= $address->full_name ?></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">ที่อยู่</th>
                                        <td><?= $address->address ?></td>
                                    </tr>
                                    </tbody>
                                </table>
                            <?php else: ?>
                                <?php echo 'ไม่พบข้อมูลที่อยู่หลัก !'; ?>
                            <?php endif; ?>
                        </div> <!-- card-body.// -->
                    </div> <!-- card.// -->

                    <div class="card">
                        <div class="card-body">
                            <!--                    <dl class="dlist-align">-->
                            <!--                        <dt>ทั้งหมด:</dt>-->
                            <!--                        <dd class="text-right">$69.97</dd>-->
                            <!--                    </dl>-->
                            <dl class="dlist-align">
                                <dt>ราคารวมทั้งหมด:</dt>
                                <dd class="text-right text-dark b"><strong>{{Total | formatNumber}}</strong></dd>
                            </dl>
                            <hr>
                            <button href="#" class="btn btn-primary btn-block" @click="payment_set=true"
                                    v-if="addressJson!=null"> สั่งซื้อสินค้า
                            </button>
                            <a href="<?= Yii::$app->homeUrl ?>" class="btn btn-light btn-block">เลือกสินค้าเพิ่ม</a>
                        </div> <!-- card-body.// -->
                    </div> <!-- card.// -->

                </aside> <!-- col.// -->
            </div>
            <div class="row" v-if="payment_set">
                <aside class="col-lg-8">
                    <div class="card">
                        <div class="card-body border-top">
                            <h4>วิธีการชำระเงิน</h4>

                            <ul class="nav nav-tabs">
                                <li class="nav-item">
                                    <a class="nav-link active" data-toggle="tab" href="#home">โอนธนาคาร</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" data-toggle="tab" href="#menu1">โอนพร้อมเพย์</a>
                                </li>

                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane container active" id="home">
                                    <br>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>เลือกธนาคาร</label>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="row">
                                                <div class="col" v-for="bk in bank">
                                                    <input type="radio" :id="bk.id" :value="bk.id" v-model="bank_id">
                                                    <label :for="bk.id">{{bk.bank_name}}</label>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <table class="table table-md table-striped table-inverse table-responsive  mb-4  "
                                           width="100%">
                                        <thead class="thead-inverse" v-for="model in findBank">
                                        <tr>
                                            <th>ธนาคาร</th>
                                            <th colspan="2">{{model.bank_name}}</th>
                                        </tr>
                                        <tr>
                                            <th>ชื่อบัญชื่</th>
                                            <th colspan="2">{{model.name}}</th>
                                        </tr>
                                        <tr>
                                            <th>เลขที่บัญชี</th>
                                            <th colspan="2">{{model.number_bank}}</th>
                                        </tr>
                                        <tr>
                                            <th>จำนวนเงินที่ต้องโอน</th>
                                            <th><strong class="text-danger ">{{Total | formatNumber}} </strong> บาท</th>
                                        </tr>
                                        </thead>
                                    </table>
                                    <br>
                                    <div class="text-right">
                                        <button class="btn btn-outline-success text-right" :disabled="bank_id==null"
                                                @click="handlePay"> ตกลง
                                        </button>
                                    </div>
                                </div>
                                <div class="tab-pane container fade" id="menu1">
                                    <br>
                                    <div class="row">
                                        <div class="col-md-2">
                                            <label>เลือก</label>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="row">
                                                <div class="col" v-for="bk in bankPay">
                                                    <input type="radio" :id="bk.id" :value="bk.id" v-model="bank_id">
                                                    <label :for="bk.id"> {{bk.name}} ( {{bk.number_bank}} ) </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="text-right">
                                        <button class="btn btn-outline-success text-right" :disabled="bank_id==null"
                                                @click="handlePay"> ตกลง
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div> <!-- card-body.// -->
                    </div> <!-- card.// -->
                </aside> <!-- col.// -->
                <aside class="col-lg-4" v-if="select_bank">
                    <div class="card mb-4">
                        <div class="card-body">
                            <div class="text-center">
                                <h6 class="text-primary">ระยะเวลาในการโอน 10 นาที</h6>
                                <h1 class="text-danger display-4">{{time}}</h1>
                            </div>
                            <hr>

                            <div class="row">
                                <div class="col-6">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <button class="btn btn-outline-secondary btn-sm" type="button"><i
                                                        class="fa fa-clock"></i></button>
                                        </div>
                                        <select class="custom-select" v-model="payment.hours">
                                            <option selected>เลือกชั่วโมง...</option>
                                            <option :value="hr.hours" v-for="hr in hoursArray">{{hr.hours}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <button class="btn btn-outline-secondary btn-sm" type="button"><i
                                                        class="fa fa-clock"></i></button>
                                        </div>
                                        <select class="custom-select" :disabled="payment.hours==null"
                                                v-model="payment.time">
                                            <option selected>เลือกนาที...</option>
                                            <option :value="mt.time" v-for="mt in minutesArray">{{mt.time}}</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <textarea class="form-control" rows="3" placeholder="หมายเหตุ"
                                              v-model="payment.remark">

                                    </textarea>
                                </div>
                            </div>
                            <hr>
                            <button class="btn btn-success btn-block" @click="saveCart" :disabled="check_btn(payment)">
                                ยืนยันการชำระ
                            </button>
                            <a href="<?= Yii::$app->homeUrl ?>" class="btn btn-light btn-block">เลือกสินค้าเพิ่ม</a>
                        </div> <!-- card-body.// -->
                    </div> <!-- card.// -->

                </aside> <!-- col.// -->
            </div>
        </div>

        <div class="text-center" v-else v-cloak>
            <h2><span class="fa fa-shopping-cart"></span> ไม่พบสินค้า</h2>
            <a href="<?= Yii::$app->homeUrl ?>" class="btn btn-outline-success">เลือกซื้อสินค้า</a>
        </div>
    </div>
    <script src="https://momentjs.com/downloads/moment.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
<?php
$bank = \yii\helpers\Json::encode(\app\models\BankPayment::find()->where(['bank_type' => 1])->all());
$bankPay = \yii\helpers\Json::encode(\app\models\BankPayment::find()->where(['bank_type' => 2])->all());
$addressJson = \yii\helpers\Json::encode($address);
$JS = '
var csrfToken = $(\'meta[name="csrf-token"]\').attr("content");
Vue.filter("formatNumber", function (value) {
    return numeral(value).format("0,0"); //
});
let app_cart_list = new Vue({
    el:"#cart_list",
    store,
    data:{
        bank_id:null,
        payment:{
            hours:null,
            time:null,
            remark:null
        },
        addressJson:' . $addressJson . ',
         bank:' . $bank . ',
         bankPay:' . $bankPay . ',
        date: moment(60 * 10 * 1000),
        dataProduct: [],
        payment_set:false,
        pay:false,
        select_bank:false,
        minutes:[],
        hours:[]
    },
    mounted(){
       this.dataProduct =this.$store.getters.getDataCart;
       for(var h=1; h <=  24 ;h++){
        let jsonH = {}
        jsonH ={
            hours : h
        }
             this.hours.push(jsonH)
        }
      
        for(var i =1 ; i< 60 ;i++){
        let json ={
            time: i
        }
            this.minutes.push(json)
        }
    },
    computed:{
    minutesArray(){
        var d = new Date();
        var m = d.getMinutes();
        return this.minutes.filter(item=>item.time > m-3)
    },
     hoursArray(){
        var d = new Date();
        var h = d.getHours();
       return  this.hours.filter((item)=>{
         if(item.hours > h-2){
            if(item.hours ==24) { item.hours ="00"}
            return item
          }
         })
    },
     time: function(){
      return this.date.format(\'mm:ss\');
    },
    findBank(){
        return this.bank.filter(item=>item.id == this.bank_id);
    },
    Total(){
            return this.dataResult.reduce((sum, item) => {
                return sum + (item.amount * item.price)
            }, 0)
    },
         dataResult(){
            let vm  = this;
            return vm.$store.getters.getDataCart
        }
    },
    methods: {
    check_btn(item){
        if(item.hours && item.time){
            return false;
        }else{
            return true;
        }
    },
    saveCart(){
    let item ={
        dataCart: this.dataResult,
        payment:this.payment,
        bank_id:this.bank_id
    }
       return  jQuery.post("/orders/save-cart",{item,_csrf: csrfToken},function(res){
        if(res.data == "ok"){
        Swal.fire({
                        type: "success",
                        title: "ชำระสำเร็จ !",
                        showConfirmButton: false,
                        timer: 1500
          })
            window.location.href="/site/profile?show=sub";
        }
         })
    },
    handlePay(){
     this.select_bank = true;
     let interval = setInterval(() => {
        let date  = moment(this.date.subtract(1, \'seconds\'))
         let time = date.format(\'mm:ss\');
          this.date = moment(this.date.subtract(0, \'seconds\'));
            if(time === "00:00"){
              this.payment_set = false;
              this.select_bank = false;
              this.bank_id = null;
              this.date = moment(60 * 10 * 1000),
              this.payment={
                hours:null,
                time:null,
                remark:null
             }
             console.log("ok",time);        
           clearInterval(interval); 
          }
       }, 1000);
    },
    
    inputAmount(model){
       if( model.amount > model.amountBalance){
            model.amount = model.amountBalance;
       }else{
        model.amount = 1
       }
          model["amount_set"] = model.amount ;
          this.$store.commit("ADD_AMOUNT", model);
    },
    isNumber: function(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
          if ((charCode > 31 && (charCode < 48 || charCode > 57)) && charCode !== 46) {
            evt.preventDefault();;
          } else {
            return true;
          }
    },
        removeCart(model,index){
            let vm  = this;
             let con = confirm("ต้องการลบรายการใช่หรือใหม่ !");
           if(con){
                let modelSend = {
                    index:index,
                    items:model
                }
               vm.$store.commit("REMOVE_CART",modelSend)
           }
        },
         addAmount(model){
                if( model.amount < model.amountBalance) model.amount++;
           model["amount_set"] = model.amount ;
          this.$store.commit("ADD_AMOUNT", model);
            },
        minAmount(model){
         if( model.amount > 1 ) model.amount--;
          model["amount_set"] = model.amount ;
          this.$store.commit("ADD_AMOUNT", model);
        },
      }
    })
';
$this->registerJs($JS);

?>