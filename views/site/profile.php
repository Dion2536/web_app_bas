<?php

use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

?>

<div class="container">
    <br>
    <div class="row">
        <div class="col-md-3 col-sm-6">
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">ข้อมูลส่วนตัว</h5>
                    <ul class="list-menu">
                        <li ><a href="<?= Url::to(['site/profile', 'show' => 'sub']) ?>"><i class="fa fa-bell"></i> ติดตามสินค้า <span
                                        class="badge badge-pill badge-light float-right"></span> </a></li>
                        <li><a href="<?= Url::to(['site/profile', 'show' => 'profile']) ?>"> <i class="fa fa-id-card"></i> ประวัติข้อมูลส่วนตัว <span
                                        class="badge badge-pill badge-light float-right"></span></a></li>
                        <li><a href="<?= Url::to(['site/profile', 'show' => 'address']) ?>"> <i class="fa fa-map-marker"></i> ที่อยู่ <span
                                        class="badge badge-pill badge-light float-right"></span> </a></li>
                        <li><a href="<?= Url::to(['site/profile', 'show' => 'history-cart']) ?>"><i class="fa fa-shopping-cart"></i> ประวัติการสั่งซื้อ <span class="badge badge-pill badge-light float-right "></span>
                            </a></li>
                        <li><a href="#"><i class="fa fa-plus-square"></i> ประวัติการสั่งทำ <span class="badge badge-pill badge-light float-right"></span>
                            </a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-9">
            <div class="card">
                <div class="card-body">
                    <?php if (Yii::$app->request->get('show') == 'sub'): ?>
                        <h5>ติดตามสินค้า</h5>
                        <?=
                         $this->render('subscribe_cart')
                        ?>
                    <?php endif; ?>
                    <?php if (Yii::$app->request->get('show') == 'history-cart'): ?>
                        <h5>ประวัติการส่ังซื้อ</h5>
                        <?=
                        $this->render('history_cart')
                        ?>
                    <?php endif; ?>

                    <?php if (Yii::$app->request->get('show') == 'profile'): ?>
                        <h5>ข้อมูลของฉัน</h5>
                    <?php if(!$profile->full_name):?>
                        <div class="alert alert-danger alert-dismissible  " role="alert">
                            <strong>  <i class="fa fa-info-circle"></i> กรุณากำหนด ข้อมูลส่วนตัวของท่านก่อน !</strong>
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    <?php endif;?>
                        <span class="text-gray">จัดการข้อมูลส่วนตัวคุณเพื่อความปลอดภัยของบัญชีผู้ใช้นี้</span>

                        <?php $form = ActiveForm::begin(); ?>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="form-group">
                                    <?= $form->field($profile, 'full_name')->textInput(); ?>
                                </div>
                                <div class="form-group">
                                    <?php
                                    echo $form->field($profile, 'birthday')->widget(\kartik\date\DatePicker::classname(), [
                                        'options' => ['placeholder' => 'Enter birth date ...'],
                                        'pluginOptions' => [
                                            'autoclose' => true,
                                            'format' => 'yyyy-m-d'
                                        ]
                                    ]);
                                    ?>
                                </div>
                                <div class="form-group">
                                    <?= $form->field($profile, 'phone')->textInput(['type' => 'number']) ?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="alert alert-success alert-dismissible  " role="alert">
                                    รหัสสมาชิก <strong class="text-danger"><?=@\app\models\Profile::findOne(['user_id'=>Yii::$app->user->id])->member_code?></strong>
                                    </div>
                                    <div class="form-group">
                                    <?= $form->field($profile, 'sex')->radioList(array(1 => 'ชาย', 2 => 'หญิง')); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <?= Html::submitButton('อัพเดทข้อมูล', ['class' => 'btn btn-success']) ?>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    <?php endif; ?>
                    <?php if (Yii::$app->request->get('show') == 'address'): ?>
                        <?= $this->render('_address') ?>
                    <?php endif; ?>
                </div>
            </div>

        </div>
    </div>
</div>



