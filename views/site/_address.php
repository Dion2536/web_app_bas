<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use app\models\Address;
$user_id = Yii::$app->user->id;
if($id = Yii::$app->request->get('id')){
    $model = Address::findOne(['id'=>$id]);
}else{
    $model = new Address();

}
if($model->load(Yii::$app->request->post()) ){
   $model->user_id= $user_id;
    $model->save();
     return  Url::to(['site/profile','show'=>'address']);
}
?>

<div class="text-right">



</div>
<div class="row">

    <div class="col-md-8">
        <h5>ที่อยู่ของฉัน</h5>
        <?php foreach (Address::find()->where(['user_id'=>$user_id])->orderBy('status') ->all()as $item ):?>
            <div class="box">
                <!-- itemside // -->
                <div class="itemside mb-3">
                    <div class="aside">
                        <i class="icon-xs bg-success rounded-circle fa fa-user white"></i>
                    </div>
                    <div class="info">
                        <small class="text-muted">ชื่อ-นามสกุล</small>
                        <p><?=$item->full_name?></p>
                    </div>
                </div>
                <div class="itemside  mb-3">
                    <div class="aside">
                        <i class="icon-xs bg-secondary rounded-circle fa fa-phone white"></i>
                    </div>
                    <div class="info">
                        <small class="text-muted">เบอร์โทร</small>
                        <p><?=$item->phone?></p>
                    </div>
                </div><!-- itemside // -->



                <div class="itemside">
                    <div class="aside">
                        <i class="icon-xs bg-danger rounded-circle fa fa-file white"></i>
                    </div>
                    <div class="info">
                        <small class="text-muted">ที่อยู่</small>
                        <p><?=$item->address?></p>
                    </div>
                </div><!-- itemside // -->

                <div class="text-right">
                    <a href="<?=Url::to(['site/profile','show'=>'address','id'=>$item->id])?>">  <span class="text-warning">แก้ไข</span> </a>
                    <?php if($item->status !=1):?>
                        <a href="<?=Url::to(['site/set-status-address','id'=>$item->id])?>"> <span class="text-info">ตั้งเป็นที่อยู่หลัก</span> </a>
                    <a href="<?=Url::to(['site/delete-address','id'=>$item->id])?>"><span class="text-danger">ลบ</span></a>
                    <?php endif;?>

                        <?php if($item->status ==1):?>
                            <div  disabled class="btn btn-sm btn-outline-success float-left"> <i class="fa fa-check"></i> ใช่้เป็นที่อยู่หลัก</div>
                        <?php endif;?>

                </div>
            </div> <!-- box.// -->
            <?php endforeach;?>
    </div>
    <div class="col-md-4">
        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'full_name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'phone')->textInput(['maxlength' => 10]) ?>
        <?= $form->field($model, 'status')->checkbox(['maxlength' => true,'value'=>1])?>
        <?= $form->field($model, 'address')->textarea(['maxlength' => true,'rows'=>3]) ?>


        <div class="form-group">
            <?= Html::submitButton($id?'<span class="fa fa-edit"></span> แก้ไขที่อยู่':'<span class="fa fa-plus"></span> เพิ่มที่อยู่', ['class' => 'btn btn-outline-success btn-block']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>