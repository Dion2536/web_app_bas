<?php
$this->title = 'สินค้าทั้งหมด'
?>
<style>
    .product_name{
        height: 37px;
        line-height: 20px;
        display: -webkit-box;
        -webkit-box-orient: vertical;
        -webkit-line-clamp: 2;
        overflow: hidden;
        margin-bottom: 4px;
        font-size: 18px;
    }
</style>
<section class="section-pagetop bg-gradient-orange">
    <div class="container">
        <h2 class="title-page"><?=\yii\helpers\Html::encode($this->title)?> (<?=Yii::$app->name?>)</h2>
    </div>

</section>

<section class="section-content padding-y" id="app-product" v-cloak>
    <div class="container">

        <div class="row">
            <aside class="col-md-3">


                <div class="card">
                    <article class="card-group-item">
                        <header class="card-header"><h6 class="title">ประเภทสินค้าทั้งหมด </h6></header>
                        <div class="filter-content">
                            <div class="list-group list-group-flush" v-for="type in typesProduct">
                                <a href="#" class="list-group-item" @click="filterType(type.id)">{{type.name}}<span class="float-right badge badge-secondary round">{{type.count}}</span> </a>
                            </div>  <!-- list-group .// -->
                        </div>
                    </article> <!-- card-group-item.// -->
                </div> <!-- card.// -->


            </aside> <!-- col.// -->
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-4 col-sm-6">

                    </div>
                </div>

                <header class="border-bottom mb-4 pb-3">
                    <div class="form-inline">
                        <span class="mr-md-auto">{{filteredProductList.length}} รายการ </span>

                        <div class="btn-group">
                            <a href="#" class="btn btn-outline-secondary" :class="{active:!product_list}"  @click="product_list=false" data-toggle="tooltip" title="" data-original-title="List view">
                                <i class="fa fa-bars"></i></a>
                            <a href="#" class="btn  btn-outline-secondary "  :class="{active:product_list}"   @click="product_list=true" data-toggle="tooltip" title="" data-original-title="Grid view">
                                <i class="fa fa-th"></i></a>
                        </div>
                    </div>
                </header><!-- sect-heading -->

                <div class="row" v-if="filteredProductList.length > 0">
                    <div class="col-md-4 col-sm-6" v-for="model in filteredProductList" v-if="product_list">
                        <a href="#" class="card card-product-grid" @click="items_details(model.id)">
                            <div class="img-wrap"> <img :src="model.photos"> </div>
                            <figcaption class="info-wrap">
                                <p class="title text-truncate">{{model.name}}</p>
                                <small class="text-muted"></small>
                                <div class="price mt-2">{{model.price |formatNumber}} บาท</div> <!-- price-wrap.// -->
                                <del class="price-old" v-if="model.price_old">{{model.price_old}} บาท.</del>
                                <div href="#" @click="items_details(model.id)" class="btn btn-outline-warning float-right"> <i class="fas fa-shopping-cart"></i> สั่งซื้อสินค้า</div>
                            </figcaption>
                        </a> <!-- card // -->

                    </div> <!-- col // -->


                    <div class="card" v-if="!product_list" >

                        <article class="card card-product-list" v-for="model in filteredProductList">
                            <div class="row no-gutters">
                                <aside class="col-md-3">
                                    <a href="#" class="img-wrap">
                                        <span class="badge badge-danger"> NEW </span>
                                        <img :src="model.photos">
                                    </a>
                                </aside> <!-- col.// -->
                                <div class="col-md-6">
                                    <div class="info-main">
                                        <a href="#" class="h5 title" @click="items_details(model.id)"> {{model.name}} </a>
                                        <div class="rating-wrap mb-3">
                                            <ul class="rating-stars">

                                            </ul>
                                        </div> <!-- rating-wrap.// -->

                                        <span v-html="model.details"> </span>
                                    </div> <!-- info-main.// -->
                                </div> <!-- col.// -->
                                <aside class="col-sm-3">
                                    <div class="info-aside">
                                        <div class="price-wrap">
                                            <span class="price h5"> {{model.price | formatNumber}} บาท</span>
                                            <del class="price-old"> </del>
                                        </div> <!-- info-price-detail // -->
                                        <p class="text-success"></p>
                                        <br>
                                        <p>
                                            <a href="#" class="btn btn-warning btn-block" @click="items_details(model.id)"><i class="fas fa-shopping-cart"></i> สั่งซื้อสินค้า </a>
                                        </p>
                                    </div> <!-- info-aside.// -->
                                </aside> <!-- col.// -->
                            </div> <!-- row.// -->
                        </article>

                    </div> <!-- card.// -->

                </div> <!-- row.// -->
                <h4 class="display-4 text-center" v-else>
                    ไม่พบรายการสินค้า !
                </h4 >



            </div> <!-- col.// -->

        </div>

    </div> <!-- container .//  -->
</section>
<script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>
<?php
$datajson = \yii\helpers\Json::encode($dataProvider);
$this->registerJs(<<<JS

 Vue.filter("formatNumber", function (value) {
    return numeral(value).format("0,0"); // displaying other groupings/separators is possible, look at the docs
});
var app = new Vue({
store,
  el: '#app-product',
  data: {
      dataProduct : $datajson,
      typesProduct :$product_type,
      product_list:true,
      message: 'Hello Vue!',
      paginate: 5,
       current: 1,
     paginate_total: 0,
     search_filter: '',
     status_filter: '',
     type_id:'',
  },
  created(){
    console.log(store.state.count)
  },
  computed: {
    filteredProductList() {
        if(this.type_id){
             return this.dataProduct.filter(post => {
         var self=this;
        return post.types_id == self.type_id  &&
        post.name.toLowerCase().indexOf(this.search_filter.toLowerCase()) >= 0
      })
        }else {
             return this.dataProduct.filter(post => {
        return post.name.toLowerCase().indexOf(this.search_filter.toLowerCase()) >= 0
      })
        }
     
    }
  },
   methods: {
      filterType(id){
          if(id){
              this.type_id = id;
          }
      },
      items_details(id)   {
         window.location.href='/site/items-details?id='+id;
      },
      updateCurrent: function (i) {
       this.current = i;
     },
     setPaginate: function (i) {
       if (this.current == 1) {
         return i < this.paginate;
       }
       else {
         return (i >= (this.paginate * (this.current - 1)) && i < (this.current * this.paginate));
       }
     },
     updatePaginate: function () {
       this.current = 1;
       this.paginate_total = Math.ceil(this.dataProduct.length/this.paginate);
     }
  }
})
JS
);

?>





















