
#### 1.ดึงข้อมูลจาก git  พิมพ์ที่ cmd
```sql
git fetch 
git pull
composer install
composer update
```

#### 2.สร้าง db และต่อฐานข้อมูล app/config/db.php
```php
    'class' => 'yii\db\Connection',
    'dsn' => 'pgsql:host=localhost;port=5432;dbname=app_db',
    'username' => 'postgres',
    'password' => 'xxxx',
    'charset' => 'utf8',
```
#### 3. migrate ตาราง 
```composer 
 php yii migrate
```

#### 4.run project

```composer 
 php yii serve 
```
- go หน้าบ้าน  http://localhost:8080/site/index
- go หลังบ้าน http://localhost:8080/manager/default/index