<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "address".
 *
 * @property int $id รหัสที่อยู่
 * @property int $user_id รหัสผู้ใช้งาน
 * @property int $status สถานะ
 * @property string $full_name ชื่อ-นามสกุล
 * @property string $phone โทรศัพท์
 * @property string $address ที่อยู่
 * @property string $created_at วันที่สร้าง
 * @property string $updated_at วันที่แก้ไข
 */
class Address extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'address';
    }
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'status'], 'default', 'value' => null],
            [['user_id', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['full_name', 'address'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'รหัสที่อยู่',
            'user_id' => 'รหัสผู้ใช้งาน',
            'status' => 'ตั้งเป็นเริ่มต้น',
            'full_name' => 'ชื่อ-นามสกุล',
            'phone' => 'โทรศัพท์',
            'address' => 'ที่อยู่',
            'created_at' => 'วันที่สร้าง',
            'updated_at' => 'วันที่แก้ไข',
        ];
    }
}
