<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "product".
 *
 * @property int $id รหัสสินค้า
 * @property string $name ชื่อสินค้า
 * @property int $types_id ประเภทสินค้า
 * @property int $status สถานะ
 * @property double $price ราคา
 * @property double $price_old ราคาก่อนหน้า
 * @property string $details รายละเอียดสินค้า
 * @property int $created_by ผู้สร้าง
 * @property int $updated_by ผู้แก้ไข
 * @property string $created_at วันที่สร้าง
 * @property string $updated_at วันที่แก้ไข
 * @property int $view จำนวนสินค้า
 */
class Product extends \yii\db\ActiveRecord
{

    public  $photos;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product';
    }

    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['types_id', 'price', 'name'], 'required'],
            [['status'], 'default', 'value' => 1],
            [['types_id', 'status', 'created_by', 'updated_by'], 'integer'],
            [['price'], 'number'],
            [['photos'], 'string'],
            [['created_at', 'updated_at','photos'], 'safe'],
            [['name', 'details'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'รหัสสินค้า',
            'name' => 'ชื่อสินค้า',
            'types_id' => 'ประเภทสินค้า',
            'photos_id' => 'รหัสรูปภาพ',
            'status' => 'สถานะ',
            'price' => 'ราคา',
            'typesName' => 'ประเภทสินค้า',
            'details' => 'รายละเอียดสินค้า',
            'created_by' => 'ผู้สร้าง',
            'updated_by' => 'ผู้แก้ไข',
            'created_at' => 'วันที่สร้าง',
            'updated_at' => 'วันที่แก้ไข',
            'sumAmount'=>'จำนวนสินค้า'
        ];
    }

    public function getTypes()
    {
        return $this->hasOne(Types::className(), ['id' => 'types_id']);
    }

    public function getTypesName()
    {
        return $this->types->name;
    }

    public function getSumAmount()
    {
        return Movement::find()->where(['product_id'=>$this->id])->sum('amount');
    }


}
