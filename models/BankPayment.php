<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "bank_payment".
 *
 * @property int $id รหัสธนาคาร
 * @property string $name ชื่อบัญชีธนาคาร
 * @property string $bank_name ชื่อธนาคาร
 * @property string $number_bank เลขบัญชี
 * @property int $bank_type รูปแบบธนาคาร
 * @property int $status สถานะ
 */
class BankPayment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bank_payment';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['bank_type', 'status'], 'default', 'value' => null],
            [['bank_type', 'status'], 'integer'],
            [ ['number_bank'],  'string', 'max' => 13],
            [['name', 'bank_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'รหัสธนาคาร',
            'name' => 'ชื่อบัญชีธนาคาร',
            'bank_name' => 'ชื่อธนาคาร',
            'number_bank' => 'เลขบัญชี',
            'bank_type' => 'รูปแบบธนาคาร',
            'status' => 'สถานะ',
        ];
    }
}
