<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "order_details".
 *
 * @property int $id รหัสรายละเอียดสั่งซื้อ
 * @property int $order_id รหัสสั่งซื้อ
 * @property int $product_id รหัสสินค้า
 * @property int $status สถานะ
 * @property int $amount จำนวน
 * @property int $created_by ผู้สร้าง
 * @property int $updated_by ผู้แก้ไข
 * @property string $created_at วันที่สร้าง
 * @property string $updated_at วันที่แก้ไข
 */
class OrderDetails extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_details';
    }
    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'product_id', 'status', 'amount', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['order_id', 'product_id', 'status', 'amount', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'รหัสรายละเอียดสั่งซื้อ',
            'order_id' => 'รหัสสั่งซื้อ',
            'product_id' => 'รหัสสินค้า',
            'status' => 'สถานะ',
            'amount' => 'จำนวน',
            'created_by' => 'ผู้สร้าง',
            'updated_by' => 'ผู้แก้ไข',
            'created_at' => 'วันที่สร้าง',
            'updated_at' => 'วันที่แก้ไข',
        ];
    }
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
    public function getProductName()
    {
        return $this->product->name;
    }

    public static function getPriceOrder($id)
    {
        $total = 0;
      $od=  OrderDetails::find()->where(['order_id'=>$id])->all();
      foreach ( $od as $md){
          $total += Product::findOne(['id'=>$md->product_id])->price;
      }
      return number_format($total,0);
    }
}
