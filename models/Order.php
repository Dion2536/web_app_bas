<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "order".
 *
 * @property int $id รหัสสั่งซื้อ
 * @property int $type_order รูปแบบสั่งซื้อ
 * @property int $status สถานะ
 * @property int $created_by ผู้สร้าง
 * @property int $updated_by ผู้แก้ไข
 * @property string $created_at วันที่สร้าง
 * @property string $updated_at วันที่แก้ไข
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order';
    }
    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type_order', 'status', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['type_order', 'status', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'รหัสสั่งซื้อ',
            'type_order' => 'รูปแบบสั่งซื้อ',
            'status' => 'สถานะ',
            'created_by' => 'ผู้สร้าง',
            'updated_by' => 'ผู้แก้ไข',
            'created_at' => 'วันที่สร้าง',
            'updated_at' => 'วันที่แก้ไข',
            'createBy.username'=>'ผู้สั่งสินค้า',
            'statusName'=>'สถานะ'
        ];
    }
    public function getCreateBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
    public function getItemStatus()
    {
        return [
            1 => 'สั่งสินค้า',
        ];
    }
    public function getStatusName(){
        return ArrayHelper::getValue($this->getItemStatus(),$this->status);
    }
}
