<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "shipping".
 *
 * @property int $id รหัสส่งของ
 * @property int $order_id รหัสสั่งสินค้า
 * @property int $user_id ชื่อผู้ส่ง
 * @property string $date_send ระยะเวลาส่ง
 * @property int $status สถานะ
 * @property string $details รายละเอียด
 * @property string $created_at วันที่สร้าง
 * @property string $updated_at วันที่แก้ไข
 * @property int $mate_order_id
 * @property int $type_shipping
 */
class Shipping extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'shipping';
    }
    public function behaviors()
    {
        return [
//            BlameableBehavior::className(),
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'user_id', 'status'], 'default', 'value' => null],
            [['order_id', 'user_id', 'status'], 'integer'],
            [['date_send', 'details'], 'string'],
            [['created_at', 'updated_at','type_shipping','mate_order_id'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'รหัสส่งของ',
            'order_id' => 'รหัสสั่งสินค้า',
            'user_id' => 'ผู้รับของ',
            'date_send' => 'ระยะเวลาส่ง',
            'status' => 'สถานะ',
            'details' => 'รายละเอียด',
            'created_at' => 'วันที่สั่ง',
            'typeShipping' => 'ประเภทการสั่ง',
            'updated_at' => 'วันที่แก้ไข',
            'statusName'=>'สถานะ'
        ];
    }

    public static function itemsAlias($key){

        $items = [
            'type'=>[
                1 => 'สั่งซื้อสินค้า',
                2 => 'สั่งทำสินค้า',
            ],
            'status'=>[
                1 => '<span class="label label-danger">รอจัดส่ง</span>',
                2 => '<span class="label label-warning">กำลังจัดส่ง...</span>',
                3 => '<span class="label label-success">จัดส่งสำเร็จ</span>.',
            ]
        ];
        return ArrayHelper::getValue($items,$key,[]);
    }
    public function getItemTypeShipping()
    {
        return self::itemsAlias('type');
    }
    public function getTypeShipping(){
        return ArrayHelper::getValue($this->getItemTypeShipping(),$this->type_shipping);
    }
    public function getItemStatus()
    {
        return self::itemsAlias('status');
    }
    public function getStatusName(){
        return ArrayHelper::getValue($this->getItemStatus(),$this->status);
    }
}
