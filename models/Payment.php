<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "payment".
 *
 * @property int $id รหัสชำระ
 * @property int $order_id รหัสสั่งสินค้า
 * @property string $bank_id รหัสธนาคาร
 * @property int $status สถานะ
 * @property string $check_time เวลาชำระ
 * @property int $created_by ผู้สร้าง
 * @property int $updated_by ผู้แก้ไข
 * @property string $created_at วันที่สร้าง
 * @property string $updated_at วันที่แก้ไข
 * @property string $remark หมายเหตุ
 * @property  int $mate_order_id
 * @property  int $type_payment
 */
class Payment extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payment';
    }

    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'status', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['order_id', 'status', 'created_by', 'updated_by'], 'integer'],
            [['created_at', 'updated_at','bank_id'], 'safe'],
            [['remark'], 'string'],
            [[ 'check_time'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'รหัสชำระ',
            'order_id' => 'รหัสสั่งสินค้า',
            'bank_id' => 'รหัสธนาคาร',
            'status' => 'สถานะ',
            'check_time' => 'เวลาชำระ',
            'created_by' => 'ผู้สร้าง',
            'updated_by' => 'ผู้แก้ไข',
            'created_at' => 'วันที่สร้าง',
            'updated_at' => 'วันที่แก้ไข',
            'remark' => 'หมายเหตุ',
            'type_payment'=>'ประเภทรายการ'
        ];
    }
    public function getBankPayment()
    {
        return $this->hasOne(BankPayment::className(), ['id' => 'bank_id']);
    }
    static public function getStatusName($status){
        $statusName = [
            '',
            'รอตรวจสอบ',
            'ตรวจสอบแล้ว',
            'ยกเลิก',
             'ยกเลิกทำรายการไม่ถูกต้อง'
        ];
        return $statusName[$status];
    }


}
