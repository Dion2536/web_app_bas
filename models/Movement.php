<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "movement".
 *
 * @property int $id รหัสการเคลื่อนไหว
 * @property int $product_id รหัสสินค้า
 * @property int $type_movement ประเภทการเคลื่อนไหว
 * @property int $amount จำนวน
 * @property string $details รายละเอียด
 * @property int $status สถานะ
 * @property int $created_by ผู้สร้าง
 * @property int $updated_by ผู้แก้ไข
 * @property string $created_at วันที่สร้าง
 * @property string $updated_at วันที่แก้ไข
 * @property integer $order_id
 */
class Movement extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'movement';
    }

    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'type_movement', 'amount', 'status', 'created_by', 'updated_by'], 'default', 'value' => null],
            [['product_id', 'type_movement', 'amount'], 'integer'],
            [['product_id', 'amount'], 'required'],
            [['created_at', 'updated_at','order_id'], 'safe'],
            [['details'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'รหัสการเคลื่อนไหว',
            'product_id' => 'รหัสสินค้า',
            'type_movement' => 'ประเภทการเคลื่อนไหว',
            'amount' => 'จำนวน',
            'details' => 'รายละเอียด',
            'status' => 'สถานะ',
            'created_by' => 'ผู้สร้าง',
            'updated_by' => 'ผู้แก้ไข',
            'created_at' => 'วันที่สร้าง',
            'updated_at' => 'วันที่แก้ไข',
        ];
    }
static  public function getNotConfirm($id){
    $model = Payment::findOne(['order_id'=>$id]);
    $model->status= 4; //ยกเลิก

    $order = Order::findOne(['id'=>$id]);
    $order->type_order = 3; //ประเภทยกเลิก
    $order->status = 4 ; //ยกเลิก
  return  $order->save() && $model->save();
}
    static public function getMateOrderMovement($mate_id){
        $payment = Payment::findOne(['mate_order_id' => $mate_id]);
        $payment->type_payment = 2 ; // ประเภทสินค้า
        $payment->status = 2; // ตรวจสอบแล้ว
        $payment->save();
        $mate = MateOrder::findOne(['id'=>$mate_id]);
        $mate->status = 2; //ตรวจสอบแล้ว
        return $mate->save();
    }
    static public function getCartMovement($order_id)
    {
        foreach (OrderDetails::findAll(['order_id' => $order_id]) as $model) {
            $mnt = new Movement();
            $mnt->type_movement = 2; // ขาย
            $mnt->amount = -intval($model->amount);
            $mnt ->order_id= intval($order_id);
            $mnt->product_id = $model->product_id;
            $mnt->status = 1;
            $mnt->save();
        }
        $payment = Payment::findOne(['order_id' => $order_id]);
        $payment->type_payment = 1 ; // ประเภทสินค้า
        $payment->status = 2; // ตรวจสอบแล้ว
        $payment->save();

        $order = Order::findOne(['id'=>$order_id]);
        $ship = new Shipping();
        $ship->status = 1; // กำลังส่ง
        $ship->type_shipping = 1 ; // order
        $ship->order_id = intval($order_id);
        $ship->user_id=intval($order->created_by);
        $order->status=2;
        $order->type_order = 2;
        $order->save();

        return  $ship->save();
    }
}
