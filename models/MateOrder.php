<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "mate_order".
 *
 * @property int $id รหัสสั่งทำสินค้า
 * @property string|null $name ชื่อสั่งทำสินค้า
 * @property string|null $details รายละเอียด
 * @property int|null $user_id รหัสสมาชิก
 * @property int|null $deposit_price ราคามัดจำ
 * @property int|null $price ราคาเต็ม
 * @property string|null $time_length ระยะเวลา
 * @property int|null $status สถานะ
 * @property string|null $created_at วันที่สร้าง
 * @property string|null $updated_at วันที่แก้ไข
 */
class MateOrder extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mate_order';
    }
    public  $member_code;

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    public function rules()
    {
        return [
            [['details'], 'string'],
            [['user_id', 'deposit_price', 'price', 'status'], 'default', 'value' => null],
            [['user_id', 'deposit_price', 'price', 'status'], 'integer'],
            [['created_at', 'updated_at','member_code'], 'safe'],
            [['name', 'time_length'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'รหัสสั่งทำสินค้า',
            'name' => 'ชื่อรายสั่งทำสินค้า',
            'details' => 'รายละเอียด',
            'user_id' => 'รหัสสมาชิก',
            'deposit_price' => 'ราคามัดจำ',
            'price' => 'ราคาเต็ม',
            'time_length' => 'ระยะเวลา',
            'status' => 'สถานะ',
            'member_code'=>'รหัสสมาชิก',
            'created_at' => 'วันที่สร้าง',
            'updated_at' => 'วันที่แก้ไข',
            'profileName'=>'ข้อมูลสมาชิก',
            'userCode'=>'รหัสสมาชิก',
             'statusName'=>'สถานะ'
        ];
    }

    public function getProfileName()
    {
        return Profile::findOne(['user_id'=>$this->user_id])->full_name;
    }
    public function getUserCode()
    {
        return Profile::findOne(['user_id'=>$this->user_id])->member_code;
    }

    public static function itemsAlias($key){

        $items = [
            'status'=>[
                1 => '<span class="label label-danger">รอจ่ายมัดจำ</span>',
                2 => '<span class="label label-success">รอตรวจสอบ..</span>',
                3 => '<span class="label label-success">จ่ายมัดจำแล้ว..</span>',
                4 => '<span class="label label-info">กำลังดำเนินการ...</span>',
                5 => '<span class="label label-warning">กำลังจัดส่ง...</span>.',
                6 => '<span class="label label-success">จัดส่งสำเร็จ...</span>.',
            ]
        ];
        return ArrayHelper::getValue($items,$key,[]);
    }
    public function getItemStatus()
    {
        return self::itemsAlias('status');
    }
    public function getStatusName(){
        return ArrayHelper::getValue($this->getItemStatus(),$this->status);
    }
}
