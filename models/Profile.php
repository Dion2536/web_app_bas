<?php

namespace app\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "profile".
 *
 * @property int $id รหัสโปรไฟล์
 * @property int $user_id รหัสผู้ใช้งาน
 * @property string $full_name ชื่อ-นามสกุล
 * @property int $phone โทรศัพท์
 *  @property int $member_code รหัสสมาชิก
 * @property int $sex เพศ
 * @property string $birthday วันเกิด
 * @property string $created_at วันที่สร้าง
 * @property string $updated_at วันที่แก้ไข
 */
class Profile extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            //BlameableBehavior::className(),
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    public static function tableName()
    {
        return 'profile';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'phone', 'sex'], 'default', 'value' => null],
            [['user_id', 'phone', 'sex'], 'integer'],
            [['birthday', 'created_at', 'updated_at','member_code'], 'safe'],
            [['full_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'รหัสโปรไฟล์',
            'user_id' => 'รหัสผู้ใช้งาน',
            'full_name' => 'ชื่อ-นามสกุล',
            'phone' => 'โทรศัพท์',
            'sex' => 'เพศ',
            'birthday' => 'วันเกิด',
            'created_at' => 'วันที่สร้าง',
            'updated_at' => 'วันที่แก้ไข',
            'member_code'=>'รหัสสมาชิก'
        ];
    }

    public static function itemsAlias($key){

        $items = [
            'sex'=>[
                1 => 'ชาย',
                2 => 'หญิง',
            ],
        ];
        return ArrayHelper::getValue($items,$key,[]);
    }
    public function getItemSex()
    {
        return self::itemsAlias('sex');
    }
    public function getSexName(){
        return ArrayHelper::getValue($this->getItemSex(),$this->sex);
    }
}
