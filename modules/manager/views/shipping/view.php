<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\form\ActiveForm;

/* @var $this yii\web\View */

/* @var $model app\models\Shipping */

use kartik\daterange\DateRangePicker;

$this->title = 'จัดการส่งสินค้า ';
$this->params['breadcrumbs'][] = ['label' => 'Shippings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$profile = \app\models\Profile::findOne(['user_id' => $model->user_id]);
$address = \app\models\Address::findOne(['user_id' => $model->user_id, 'status' => 1]);
\yii\web\YiiAsset::register($this);
$addon = <<< HTML
<div class="input-group-append">
    <span class="input-group-text">
        <i class="fas fa-calendar-alt"></i>
    </span>
</div>
HTML;
?>
<div class="shipping-view">
    <h4><?= Html::encode($this->title) ?></h4>
    <div class="text-right">
        <?= Html::a('<i class="fa fa-file"></i> พิมพ์ใบส่งของ', ['shipping/print-bill', 'id' => $model->id], ['class' => 'btn btn-info']) ?>
        <?php if($model->status !=3):?>
        <?= Html::a(' <i class="fa fa-remove"></i>  ยกเลิกส่ง', ['reset', 'id' => $model->id], ['class' => 'btn btn-danger']) ?>
        <?php endif;?>
    </div>
    <div class="row">
        <div class="col-md-6">
            <?php $form = ActiveForm::begin(); ?>

            <?=
            $form->field($model, 'date_send', [
                'addon' => ['prepend' => ['content' => '<i class="fas fa-calendar-alt"></i>']],
                'options' => ['class' => 'drp-container form-group']
            ])->widget(DateRangePicker::classname(), [
                'useWithAddon' => true,
                'pluginOptions' => [
                    'locale' => [
//                         'separator'=>'-',
                        'format' => 'Y-M-DD'
                    ],
                    'opens' => 'left'
                ]
            ]);
            ?>

            <?= $form->field($model, 'details')->textarea(['rows' => 6]) ?>


            <div class="form-group">
                <?= Html::submitButton('บันทึกอัพเดท', ['class' => 'btn btn-success','disabled' => $model->status==3]) ?>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-md-3">
            <div class="x_panel">
                <div class="x_title">
                    <h2>ข้อมูลการสั่งสินค้า</h2>

                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'order_id',
//                'mate_order_id',
                            'typeShipping',
//                            'user_id',
                            [
                                'attribute' => 'date_send',
                                'value' => function ($model) {
                                    if ($model->date_send) {

                                        $date1 = date_create(substr($model->date_send, 0, 10));
                                        $date2 = date_create(substr($model->date_send, 13, 10));
                                        $interval = date_diff($date1, $date2);
                                        return $interval->format('%a ') . "วัน";;
                                    }

                                }
                            ],
                            'statusName:raw',
                            'details:ntext',
                            'created_at',
//                'updated_at',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="x_panel">
                <div class="x_title">
                    <h2>ข้อมูลลูกค้า</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <?= DetailView::widget([
                        'model' => $profile,
                        'attributes' => [
                            // 'id',
                            'member_code',
                            'full_name',
                            'phone'
//                'updated_at',
                        ],
                    ]) ?>
                </div>
            </div>

            <div class="x_panel">
                <div class="x_title">
                    <h2>ข้อมูลที่อยู่</h2>

                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <?= DetailView::widget([
                        'model' => $address,
                        'attributes' => [
                            //    'id',
                            'full_name',
                            'phone',
                            'address'
//                'updated_at',
                        ],
                    ]) ?>
                </div>
            </div>
        </div>
    </div>

</div>
