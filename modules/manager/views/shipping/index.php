<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ShippingSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'จัดการส่งสินค้า';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="shipping-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
//            'id',
            'order_id',
            'date_send:ntext',
//            'mate_order_id',
            'typeShipping',
            'statusName:raw',
            //'details:ntext',
            'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn', 'template' => '{view} {send-success}',

                'visibleButtons' => [
                    'send-success' => function ($model) {
                        return $model->status ==2;
                    },
                ],
                'buttons' => [
                    'send-success' => function ($url, $model, $key) {     // render your custom button
                        return Html::a('<i class="fa fa-check"></i>', ['send-success','id'=>$model->id], ['class' => 'text-success', 'data-pjax' => 0]);
                    }
                ]
            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
