<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PaymentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'การชำระ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
//            'order_id',
            [
                'attribute' => 'order_id', 'value' => function ($model) {
                if ($model->type_payment ==1) {
                    return $model->order_id;
                } else {
                    return $model->mate_order_id;
                }
            }
            ],
       [
           'attribute' => 'type_payment', 'value' => function ($model) {
           if ($model->type_payment == 1) {
               return 'สั่งทำซื้อ';
           } else {
               return 'สั่งทำ';
           }
       }
       ],
            'bankPayment.bank_name',
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return \app\models\Payment::getStatusName($model->status);
                }
            ],
            'check_time',
            'remark:ntext',
            ['class' => 'yii\grid\ActionColumn',
                'template' => '{check-mate} {check}  {cancel}',
                'visibleButtons' => [
                    'check' => function ($model) {
                        return $model->type_payment == '1' && $model->status == '1';
                    },
                    'check-mate' => function ($model) {
                        return $model->type_payment == 2 && $model->status == 1;
                    },
                    'cancel' => function ($model) {
                        return $model->status == '1';
                    }
                ],
                'buttons' => [
                    'check-mate' => function ($url, $model) {
                        return Html::a('<i class="glyphicon glyphicon-check"></i>', ['payment/check-mate', 'id' => $model['id']]);
                    },
                    'check' => function ($url, $model) {
                        return Html::a('<i class="glyphicon glyphicon-check"></i>', ['payment/check', 'id' => $model['id']]);
                    },
                    'cancel' => function ($url, $model) {
                        return Html::a('<i class="glyphicon glyphicon-remove-circle"></i>', ['payment/cancel', 'id' => $model['id']]);
                    }
                ]
            ],
            //'created_by',
            //'updated_by',
            //'created_at',
            //'updated_at',
            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
