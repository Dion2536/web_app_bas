<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'รายการสั่งซื้อสินค้า';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
//            'type_order',
            'statusName',
            'createBy.username',
//            'updated_by',
            [
                'label' => 'จำนวนรายการ',
                'value' => function ($model) {
                  return   \app\models\OrderDetails::find()->where(['order_id'=>$model->id])->count();
                }
            ],
              [
                'label' => 'ราคา',
                'value' => function ($model) {
                  return   \app\models\OrderDetails::getPriceOrder($model->id);
                }
            ]
            //'created_at',
            //'updated_at',

            //  ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>