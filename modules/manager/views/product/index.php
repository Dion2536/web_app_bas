<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ข้อมูลสินค้า';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">
    <?php

    ?>
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('สร้างข้อมูลสินค้า', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'name',
            'typesName',
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return Yii::$app->global->status($model->status);
                }
            ],
            'sumAmount',
            'price',
            //'details',
            //'created_by',
            //'updated_by',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
