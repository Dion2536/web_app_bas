<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\file\FileInput;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Product */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="product-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="text-right">
        <?= Html::a('หน้าหลัก', ['index', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </div>
    <div class="row">
        <div class="col-md-8">
            <code>
                ขนาดรูปภาพ ต้องการ 600 x 600
            </code>
            <?php $form = ActiveForm::begin([
                'options' => ['enctype' => 'multipart/form-data']
            ]); ?>
            <?php
            //        echo '<label class="control-label">Upload รูปภาพสินค้า</label>';
            echo FileInput::widget([
                'name' => 'photos',
                'model' => $photos,
                'attribute' => 'name',
            ]);
            ?>
            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-md-4">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
//            'id',
                    'name',
                    'typesName',
                    [
                        'attribute' => 'status',
                        'value' => function ($model) {
                            return Yii::$app->global->status($model->status);
                        }
                    ],
                    'sumAmount',
                    'price',
                    'details',
//            'created_by',
//            'updated_by',
                    'created_at',
                    'updated_at',
                ],
            ]) ?>
        </div>
    </div>
    <div class="row">

        <?php foreach (\app\models\Photos::find()->where(['product_id' => $model->id])->orderBy('status_use' )->all() as $ps): ?>
            <div class="col-md-3">
                <div class="x_panel">
                    <div class="x_content">
                        <img class="img-responsive avatar-view" height="100%"
                             src="<?php echo Yii::getAlias('@web') . '/uploads/' . $ps->name ?>" alt="Avatar" title="">

                        <div class="text-right">
                            <?php   if($ps->status_use !=1):?>
                            <a href="<?= \yii\helpers\Url::to(['product/status-use', 'id' => $ps->id, 'page' => $model->id]) ?>"
                               class="btn btn-success"><i class="fa fa-check-circle-o"></i></a>
                            <a href="<?= \yii\helpers\Url::to(['product/delete-photos', 'id' => $ps->id, 'page' => $model->id]) ?>"
                               class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
                            <?php else:?>
                                <a href="#" disabled
                                   class="btn btn-danger"><i class="fa fa-trash-o"></i></a>
                            <?php endif;?>

                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>

</div>
