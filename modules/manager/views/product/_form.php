<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Types;
use dosamigos\ckeditor\CKEditor;
?>

<div class="product-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?php
    echo $form->field($model, 'types_id')->widget(\kartik\select2\Select2::classname(), [
        'data' =>ArrayHelper::map(Types::find()->where(['status'=>1])->all(),'id','name'),
        'options' => ['placeholder' => 'เลือกประเภทสินค้า ...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]);
    ?>

    <?= $form->field($model, 'price')->textInput() ?>


    <?= $form->field($model, 'details')->textarea(['rows' => '6']) ?>



    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
