<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\BankPayment */
/* @var $form yii\widgets\ActiveForm */

$data = [
    'ธนาคารทหารไทย'=>'ธนาคารทหารไทย',
    'ธนาคารไทยพาณิชย์'=>'ธนาคารไทยพาณิชย์',
    'ธนาคารกรุงศรีอยุธยา'=>'ธนาคารกรุงศรีอยุธยา',
    'ธนาคารเกียรตินาคิน'=>'ธนาคารเกียรตินาคิน',
    'ธนาคารกรุงเทพ'=>'ธนาคารกรุงเทพ',
    'ธนาคารกสิกรไทย'=>'ธนาคารกสิกรไทย',
    'ธนาคารออมสิน'=>'ธนาคารออมสิน',
    'ธนาคารธนชาต'=>'ธนาคารธนชาต',
    'ธนาคารยูโอบี'=>'ธนาคารยูโอบี',
    'ธนาคารทิสโก้'=>'ธนาคารทิสโก้',
]
?>

<div class="bank-payment-form">

    <?php $form = ActiveForm::begin(); ?>
    <?=
    $form->field($model, 'bank_type')->dropDownList(
        [1 => 'ธนาคาร (Bank)', 2 => 'พร้อมเพย์ (PromptPay)']
    );

    $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?=
    $form->field($model, 'bank_name')->widget(Select2::classname(), [
        'data' => $data,
        'options' => ['placeholder' => 'พิมพ์เพิ่มหรือเลือกธนาคาร...'],
        'pluginOptions' => [
            'allowClear' => true,
            'tags' => true
        ],
    ]);
    ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'number_bank')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('บันทึกข้อมูล', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
