<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\BankPayment */

$this->title = 'สร้าง ธนาคาร';
$this->params['breadcrumbs'][] = ['label' => 'Bank Payments', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bank-payment-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
