<?php
/* @var $this yii\web\View */

use kartik\daterange\DateRangePicker;
use kartik\form\ActiveForm;
use kartik\grid\GridView;

$today = Yii::$app->formatter->asDate('now', 'yyyy-MM-dd');
$day = DateTime::createFromFormat('Y-m-d', $today);
$day->setISODate((int)$day->format('o'), (int)$day->format('W'), 2);
$week_submit = $day->format('Y-m-d');
$date_search = Yii::$app->request->get('date_search');
$value = '';
if (isset($date_search)) {
    $value = $date_search;
    $dataStart = substr($date_search, 0, 10);
    $dataEnd = substr($date_search, 13, 10);
} else {
    $dataStart = $week_submit;
    $dataEnd = $today;
}
$sql = "SELECT 
date(m.created_at) as createdNew ,p.name,
m.amount,
sum(p.price * m.amount*-1),
users.username
FROM movement m 
inner join product p on m.product_id = p.id
left join \"order\" o   on m.order_id = o.id
left join \"user\" users on o.created_by = users.id
where  date(m.created_at)  BETWEEN '$dataStart' AND '$dataEnd' and type_movement = 2
GROUP BY createdNew,p.name,users.username ,m.amount";
$dataRaw = Yii::$app->db->createCommand($sql)->queryAll();
$total = 0;
foreach ($dataRaw as $mo){
    $total+=$mo['sum'];
}
$provider = new \yii\data\ArrayDataProvider([
    'allModels' => $dataRaw,
    'pagination' => [
        'pageSize' => 10,
    ],
    'sort' => [
        'attributes' => ['id', 'name'],
    ],
]);
?>
<h1>รายงานการสั่งซื้อสินค้า</h1>

<div class="container">
    <?php
    $form = ActiveForm::begin(
        ['method' => 'get', 'action' => \yii\helpers\Url::to(['order']),
            'options' => [
                //'class'=>'form-inline'
            ],
        ]
    ); ?>
    <div class="row">

        <div class="col-md-4">
            <?php
            echo DateRangePicker::widget([
                'name' => 'date_search',
                // 'value'=>'2015-10-19 - 2015-11-03',
                'convertFormat' => true,
                'readonly' => true,
                'presetDropdown' => true,
                'language' => 'th',
                'pluginOptions' => [
                    'locale' => ['format' => 'Y-m-d']
                ],
                'pluginEvents' => [
                    // 'apply.daterangepicker' => 'function() { this.form.submit();}'
                ]
            ]);

            ?>

        </div>
        <div class="col-md-3 ">
            <div class="form-group">
                <?= \yii\helpers\Html::submitButton('<i class="fa fa-search"></i> ค้นหา', ['class' => 'btn btn-success']) ?>
            </div>
        </div>

        <?php ActiveForm::end(); ?>


    </div>
    <div class="x_panel">
        <div class="x_title">
            <h2>แสดงข้อมูลรายงาน</h2>
            <div class="clearfix"></div>
        </div>
        <div class="x_content">
            <div class="row">
                <div class="col-md-7">
                    <?= GridView::widget([
                        'dataProvider' => $provider,
                        'columns' => [
          ['class' => 'yii\grid\SerialColumn'],
                           'creatednew:text:วันที่',
                            'name:text:รายการสินค้า',
                            'sum:text:ราคา',
                             'username:text:ชื่อสมาชิก'
                            //'updated_by',
                            //'created_at',
                            //'updated_at',
                            //['class' => 'yii\grid\ActionColumn'],
                        ],
                    ]); ?>
                </div>
                <div class="col-md-4 text-center">
                    <h3>รวมรายได้</h3>
                    <h1 class="text-success"> <strong><?=number_format($total,0)?></strong></h1>
                </div>
            </div>
        </div>
    </div>
</div>