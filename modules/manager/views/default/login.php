<?php
$this->title = 'เข้าสู่ระบบจัดการร้าน'.Yii::$app->name;

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
?>

<div class="row">
    <div class="col-sm-4 ">
        <div class="card">
            <article class="card-body">
                <h3 class="card-title mb-4 mt-1"><?=\yii\helpers\Html::encode($this->title)?></h3>
                <?php $form = ActiveForm::begin([
                    'id' => 'login-form',
                ]); ?>
                <div class="form-group">
                    <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
                </div> <!-- form-group// -->
                <div class="form-group">
                    <?= $form->field($model, 'password')->passwordInput() ?>
                </div> <!-- form-group// -->
                <div class="form-group">
                    <?= $form->field($model, 'rememberMe')->checkbox() ?>
                </div> <!-- form-group// -->
                <div class="form-group">
                    <?= Html::submitButton('เข้าสู่ระบบ', ['class' => 'btn btn-primary btn-block', 'name' => 'login-button']) ?>
                </div> <!-- form-group// -->
                <?php ActiveForm::end(); ?>

            </article>
        </div> <!-- card.// -->
    </div>
</div>
