<div class="manager-default-index">
    <div class="row">
        <div class="col-xs-12 col-md-3">
            <?=
            \yiister\gentelella\widgets\StatsTile::widget(
                [
                    'icon' => 'list-alt',
                    'header' => 'สั่งซื้อ',
                    'text' => 'รายการสั่งซื้อ',
                    'number' => \app\models\Order::find()->where(['status' => 1])->count(),
                ]
            )
            ?>
        </div>
        <div class="col-xs-12 col-md-3">
            <?=
            \yiister\gentelella\widgets\StatsTile::widget(
                [
                    'icon' => 'pie-chart',
                    'header' => 'สั่งทำ',
                    'text' => 'รายการสั่งทำ',
                    'number' => \app\models\MateOrder::find()->where(['status' => [1, 2, 3, 4, 5]])->count(),
                ]
            )
            ?>
        </div>
        <div class="col-xs-12 col-md-3">

            <?=
            \yiister\gentelella\widgets\StatsTile::widget(
                [
                    'icon' => 'users',
                    'header' => 'จัดส่ง',
                    'text' => 'รายการจัดส่ง',
                    'number' => \app\models\Shipping::find()->where(['status' => 2])->count(),
                ]
            )
            ?>
        </div>
        <div class="col-xs-12 col-md-3">
            <?=
            \yiister\gentelella\widgets\StatsTile::widget(
                [
                    'icon' => 'comments-o',
                    'header' => 'สมาชิก',
                    'text' => 'สมาชิกทั้งหมด',
                    'number' => \app\models\User::find()->where(['roles' => 10])->count(),
                ]
            )
            ?>
        </div>
    </div>


</div>
