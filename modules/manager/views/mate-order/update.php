<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\MateOrder */

$this->title = 'แก้ไขข้อมูลสั่งทำ: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Mate Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="mate-order-update">

    <h3><?= Html::encode($this->title) ?></h3>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
