<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use kartik\daterange\DateRangePicker;
$profile = \app\models\Profile::findOne(['user_id'=>$model->user_id]);

?>

<div class="mate-order-form">
    <div class="row">
        <div class="col-md-4 ">
            <div class="x_panel">
                <div class="x_title">
                    <h2>ข้อมูลสมาชิก</h2>
                    <ul class="nav navbar-right panel_toolbox">
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <h2 class="text-center">ชื่อ <?=$profile->full_name?></h2>
                    <div class="form-group row">
                        <label class="col-form-label col-md-3 col-sm-3 ">รหัสสมาชิก</label>
                        <div class="col-md-9 col-sm-9 ">
                            <input type="text" class="form-control input-sm" readonly="readonly" value="<?=$profile->member_code?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-3 col-sm-3 ">เพศ</label>
                        <div class="col-md-9 col-sm-9 ">
                            <input type="text" class="form-control input-sm" readonly="readonly" value="<?=@$profile->getSexName()?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-3 col-sm-3 ">วันเกิด</label>
                        <div class="col-md-9 col-sm-9 ">
                            <input type="text" class="form-control input-sm" readonly="readonly" value="<?=@$profile->birthday?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-3 col-sm-3 ">เบอร์โทร</label>
                        <div class="col-md-9 col-sm-9 ">
                            <input type="text" class="form-control input-sm" readonly="readonly" value="<?=@$profile->phone?>">
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <div class="col-md-7">
            <div class="x_panel">
                <div class="x_title">
                    <h2>สร้างส่ังทำ</h2>
                    <ul class="nav navbar-right panel_toolbox">
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <?php $form = ActiveForm::begin(['method' => 'post']); ?>
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                    <?php $form->field($model, 'time_length')->textInput() ?>
                    <?php
                    echo $form->field($model, 'time_length', [
                        'addon'=>['prepend'=>['content'=>'<i class="fas fa-calendar-alt"></i>']],
                        'options'=>['class'=>'drp-container form-group']
                    ])->widget(DateRangePicker::classname(), [
                        //'presetDropdown'=>true,
                        'useWithAddon'=>true,
                        'initRangeExpr'=>true,

                    ]);
                    ?>
                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field($model, 'deposit_price')->textInput(['type'=>'number']) ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'price')->textInput(['type'=>'number']) ?>
                        </div>
                    </div>

                    <?= $form->field($model, 'details')->textarea(['rows' => 3]) ?>
                    <div class="form-group text-right">
                        <?= Html::submitButton('<i class="fa fa-edit"></i> บันทึกแก้ไขข้อมูล', ['class' => 'btn btn-info']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>

</div>
