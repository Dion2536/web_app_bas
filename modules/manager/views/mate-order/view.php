<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\form\ActiveForm;
use kartik\daterange\DateRangePicker;

/* @var $this yii\web\View */
/* @var $model app\models\MateOrder */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Mate Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
$profile = \app\models\Profile::findOne(['user_id' => $model->user_id]);
?>
<div class="mate-order-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="text-right">
        <?php if ($model->status == 2): ?>
        <?= Html::a('โอนมัดจำแล้ว', ['confirm', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        <?php elseif ($model->status == 3): ?>
            <?= Html::a('ดำเนินการทำสินค้า', ['create-mate', 'id' => $model->id], ['class' => 'btn btn-warning']) ?>

        <?php elseif ($model->status == 4 ): ?>
        <?= Html::a(' ดำเนินการจัดส่ง', ['send', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        <?php elseif ($model->status == 5): ?>

            <?= Html::a(' จัดส่งสำเร็จ', ['send-success', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        <?php endif; ?>
        <?= Html::a('<i class="fa fa-print"></i> พิมพ์ใบเสร็จ', ['mate-order-bill', 'id' => $model->id], ['class' => 'btn btn-info']) ?>
    </div>
    <div class="row">
        <div class="col-md-7">
            <div class="x_panel">
                <div class="x_title">
                    <h2>ข้อมูลสมาชิก</h2>
                    <ul class="nav navbar-right panel_toolbox">
                    </ul>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <h2 class="text-center">ชื่อ <?= $profile->full_name ?></h2>
                    <div class="form-group row">
                        <label class="col-form-label col-md-3 col-sm-3 ">รหัสสมาชิก</label>
                        <div class="col-md-9 col-sm-9 ">
                            <input type="text" class="form-control input-sm" readonly="readonly"
                                   value="<?= $profile->member_code ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-3 col-sm-3 ">เพศ</label>
                        <div class="col-md-9 col-sm-9 ">
                            <input type="text" class="form-control input-sm" readonly="readonly"
                                   value="<?= @$profile->getSexName() ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-3 col-sm-3 ">วันเกิด</label>
                        <div class="col-md-9 col-sm-9 ">
                            <input type="text" class="form-control input-sm" readonly="readonly"
                                   value="<?= @$profile->birthday ?>">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-form-label col-md-3 col-sm-3 ">เบอร์โทร</label>
                        <div class="col-md-9 col-sm-9 ">
                            <input type="text" class="form-control input-sm" readonly="readonly"
                                   value="<?= @$profile->phone ?>">
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-5">
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
//                'id',
                    'name',
                    'details:ntext',
                   // 'user_id',
                    'deposit_price',
                    'price',
                    'time_length',
                    'statusName:raw',
                    'created_at',
//                'updated_at',
                ],
            ]) ?>
        </div>
    </div>


</div>
