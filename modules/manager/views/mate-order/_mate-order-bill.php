<?php
$this->registerCssFile("@web/app_frontend/css/site.css", [
], 'css');
$total = 0;
?>
<style>
    table {
        width:100%;
    }
    table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
    }
    th{
        background-color: #eee;

    }
    th, td {
        padding: 8px;
        text-align: left;
    }
    .row {
        display: -ms-flexbox;
        display: flex;
        -ms-flex-wrap: wrap;
        flex-wrap: wrap;
        margin-right: -15px;
        margin-left: -15px;
    }
    .col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12 {
        position: relative;
        min-height: 1px;
        padding-right: 15px;
        padding-left: 15px;
    }
    .col-xs-1, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9, .col-xs-10, .col-xs-11, .col-xs-12 {
        float: left;
    }
    .col-xs-12 {
        width: 100%;
    }
    .col-xs-11 {
        width: 91.66666667%;
    }
    .col-xs-10 {
        width: 83.33333333%;
    }
    .col-xs-9 {
        width: 75%;
    }
    .col-xs-8 {
        width: 66.66666667%;
    }
    .col-xs-7 {
        width: 58.33333333%;
    }
    .col-xs-6 {
        width: 50%;
    }
    .col-xs-5 {
        width: 41.66666667%;
    }
    .col-xs-4 {
        width: 33.33333333%;
    }
    .col-xs-3 {
        width: 25%;
    }
    .col-xs-2 {
        width: 16.66666667%;
    }
    .col-xs-1 {
        width: 8.33333333%;
    }
</style>
<div style=" font-style: italic;">#ลำดับที่ <?=@$model->id?></div>
<h1 style="text-align: center">ใบส่งสินค้า ร้าน<?=@Yii::$app->name?></h1>
<div class="row">
    <div class="col-xs-6">
        <div style="font-size: 22px;font-weight: bold;">ที่อยู่จัดส่ง</div>
        <div style="font-size: 18px; top: -300px;z-index: 100; position: absolute;"><strong>ชื่อผู้รับ</strong> <?=@$address->full_name?> </div>
        <div style=" font-size: 18px;top: -300px;z-index: 100; position: absolute;"><strong>ที่อยู่</strong> <?=@$address->address?> </div>
        <div style=" font-size: 18px;top: -300px;z-index: 100; position: absolute;"><strong>เบอร์ติดต่อ</strong> <?=@$address->phone?> </div>

    </div>
    <div class="col-xs-3">
        <div style=" font-size: 18px;top: -300px;z-index: 100; position: absolute;font-weight: bold;">รหัสสมาชิก <span style="color: tomato">(<?=@$profile->member_code?>) </span></div>
        <div style=" font-size: 18px;top: -300px;z-index: 100; position: absolute;"><strong>ชื่อ</strong>  <?=@$profile->full_name?> </div>
    </div>
</div>
<h2>รายการสินค้า</h2>
<table class="table-bordered">
    <tr>
        <th width="5%">ลำดับ</th>
        <th width="70%">ชื่อสินค้าสั่งทำ</th>
        <th width="12%" style="text-align: center">ราคามัดจำ</th>
        <th width="12%" style="text-align: right">ราคา</th>
    </tr>
        <tr>
            <td style="text-align: center"><?=1?></td>
            <td><?=@$model->name?></td>
            <td style="text-align: right"><?=@number_format($model->deposit_price,0)?></td>
            <td style="text-align: right"><?=@number_format($model->price,0)?></td>
        </tr>
    <tr>
        <td colspan="3" style="text-align: center;font-weight: bold;">ราคารวมทั้งหมด</td>
        <td style="text-align: right;font-weight: bold;"><?=@number_format($model->price,0)?></td>
    </tr>
</table>