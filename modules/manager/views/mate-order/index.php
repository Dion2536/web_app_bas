<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\MateOrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ข้อมูลสั่งทำ';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="mate-order-index">

    <h3><?= Html::encode($this->title) ?></h3>
    <div class="text-right">
        <?= Html::a('สร้างสั่งทำ', ['create'], ['class' => 'btn btn-success']) ?>
    </div>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            // 'id',

            'userCode',
            'profileName',

            'name',
            'deposit_price',
            'price',
            'time_length',
            'statusName:raw',
            'details:ntext',
            //'status',
            //'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn',
                'contentOptions' => [
                    'noWrap' => true
                ],
                'template'=>' {view} {update} {delete}',

                'visibleButtons' => [
                    'delete' => function ($model) {
                        return $model->status === 1;
                    },
                ],

            ],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
