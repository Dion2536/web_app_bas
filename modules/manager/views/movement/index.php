<?php
$this->title = 'เพิ่มสินค้าเข้า';

use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Product;
use yii\helpers\Html;
use yii\grid\GridView;

?>


<div class="row">
    <div class="col-md-4">
        <h4>เพิ่มสินค้าเข้าระบบ</h4>
        <?php $form = ActiveForm::begin(); ?>
        <?php
        echo $form->field($model, 'product_id')->widget(\kartik\select2\Select2::classname(), [
            'data' => ArrayHelper::map(Product::find()->where(['status' => 1])->all(), 'id', 'name'),
            'options' => ['placeholder' => 'เลือกสินค้า ...'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]);
        ?>
        <?= $form->field($model, 'amount')->textInput(['type' => 'number', 'placeholder' => 'จำนวนสินค้า', 'min' => 0]) ?>
        <?= $form->field($model, 'details')->textarea(['placeholder' => 'รายละเอียดสินค้า']) ?>
        <div class="form-group">
            <?= Html::submitButton('บันทึกข้อมูล', ['class' => 'btn btn-success']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
    <div class="col-md-7"><h4>รายการนำเข้า</h4>
        <?= GridView::widget([
            'dataProvider' => $provider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

//            'id',
                'name:html:ชื่อสินค้า',
//                [
//                    'attribute' => 'status',
//                    'value' => function ($model) {
//                        return Yii::$app->global->status($model->status);
//                    }
//                ],
                'amount:html:จำนวน',
                //'details',
                //'created_by',
                //'updated_by',
                'created_at:html:วันที่',
                //'updated_at',
                [
                    'label' => 'จัดการข้อมูล',
                    'format'=>'raw',
                    'value'=>function($model){
                        return Html::a('<i class="fa fa-trash-o"></i>', ['delete', 'id' => $model['id']], ['class' => '']);
                    }
                ],
            ],
        ]); ?>
    </div>
</div>


