<?php
use yii\grid\GridView;
use yii\helpers\Html;
$this->title = 'ข้อมูลสมาชิก';
?>
<h3><?=Html::encode($this->title)?></h3>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],
        [
            'label' => 'ชื่อ-นามสกุล',
            'value' => function($model){
                return \app\models\Profile::findOne(['user_id'=>$model->id])->full_name;
            }
        ],
        'username:text:ชื่อผู้ใช้งาน',

        [
            'label' => 'รหัสสมาชิก',
            'value' => function($model){
                return \app\models\Profile::findOne(['user_id'=>$model->id])->member_code;
            }
        ],
        'email:text:อีเมล์',
        [
            'label' => 'เบอร์โทร',
            'value' => function($model){
                return \app\models\Profile::findOne(['user_id'=>$model->id])->phone;
            }
        ],
        ['class' => 'yii\grid\ActionColumn',
            'template' => ' {use-check}  {cancel}',
            'visibleButtons' => [

                'use-check' => function ($model) {
                    return  $model->status == 9;
                },
                'cancel' => function ($model) {
                    return $model->status == 10;
                }
            ],
            'buttons' => [
                'use-check' => function ($url, $model) {
                    return Html::a('<i class="fa fa-check text-success"></i> เปิดใช้งาน', ['member/use-check', 'id' => $model['id']]);
                },

                'cancel' => function ($url, $model) {
                    return Html::a('<i class="fa fa-ban text-danger"></i> ปิดใช้งาน', ['member/cancel', 'id' => $model['id']]);
                }
            ]
        ],

        //  ['class' => 'yii\grid\ActionColumn'],
    ],
]); ?>