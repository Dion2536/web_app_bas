<?php

namespace app\modules\manager;

use app\models\User;
use Yii;
use yii\helpers\Url;

/**
 * manager module definition class
 */
class Module extends \yii\base\Module
{
    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\manager\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();
        Yii::$app->user->loginUrl = ['/manager/default/login'];
        // custom initialization code goes here
    }
}
