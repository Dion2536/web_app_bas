<?php

namespace app\modules\manager\controllers;


use app\models\Movement;
use yii\data\ArrayDataProvider;
use Yii;
class MovementController extends \yii\web\Controller
{

    public $layout = 'main';

    public function actionIndex()
    {
        $model = new Movement();
        if($model->load(Yii::$app->request->post())){
           if($model->amount){
               $model->type_movement = 1 ; // add product
               $model->status = 1 ;
               $model->save();
           }
            return $this->redirect(['movement/index'])
;        }
        $dataRaw = \Yii::$app->db->createCommand('SELECT p.name ,m.* FROM movement m 
inner join product p on p.id = m.product_id
WHERE m.type_movement = 1')->queryAll();
        $provider = new ArrayDataProvider([
            'allModels' => $dataRaw,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'attributes' => ['id', 'name'],
            ],
        ]);
        return $this->render('index',[
            'model'=>$model,
            'provider'=>$provider
        ]);
    }
    public function actionDelete($id)
    {
        Movement::findOne($id)->delete();
        return $this->redirect(['movement/index']);
    }

}
