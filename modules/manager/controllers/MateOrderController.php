<?php

namespace app\modules\manager\controllers;

use app\models\Address;
use app\models\Movement;
use app\models\Payment;
use app\models\Profile;
use Mpdf\Mpdf;
use Yii;
use app\models\MateOrder;
use app\models\MateOrderSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * MateOrderController implements the CRUD actions for MateOrder model.
 */
class MateOrderController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public $layout = 'main';

    /**
     * Lists all MateOrder models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new MateOrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionConfirm($id = null)
    {
        $model = Payment::findOne(['mate_order_id' => $id]);
        if (Movement::getMateOrderMovement($model->mate_order_id)) {
            return 'ok';
        }
    }

    /**
     * Displays a single MateOrder model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new MateOrder model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new MateOrder();

        if ($model->load(Yii::$app->request->post())) {
            $get = Yii::$app->request->get();
            $profile = \app\models\Profile::findOne(['member_code' => $get['MateOrder']['member_code']]);
            $model->user_id = $profile->user_id;
            $model->status = 1;  //สร้าง
             $model->save();
            return $this->redirect(['index', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing MateOrder model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->id]);
        }
        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionCreateMate($id){
        $model = $this->findModel($id);
        $model->status = 4 ;
        $model->save();
        return $this->redirect(['view', 'id' => $model->id]);
    }
    public function actionSend($id){
    $model = $this->findModel($id);
    $model->status = 5;
    $model->save();
    return $this->redirect(['view', 'id' => $model->id]);
}
    public function actionSendSuccess($id){
        $model = $this->findModel($id);
        $model->status =6 ;
        $model->save();
        return $this->redirect(['view', 'id' => $model->id]);
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the MateOrder model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return MateOrder the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */

    public function actionMateOrderBill($id)
    {
        $model = $this->findModel($id);
        $profile = Profile::findOne(['user_id'=>$model->user_id]);
        $address = Address::findOne(['user_id'=>$model->user_id]);
        $params = Yii::$app->params;
        $fontDirs = $params['defaultConfig']['fontDir'];
        $fontData = $params['defaultFontConfig']['fontdata'];

        $html = $this->renderPartial('_mate-order-bill',['profile'=>$profile,'address'=>$address,'model'=>$model ]);

        $mpdf = new Mpdf([
            'fontDir' => array_merge($fontDirs, [
                \yii\helpers\Url::base() . 'app_frontend/fonts/sarabun/',
            ]),
            'fontdata' => $fontData + ['thsarabun' => $params['SetFontTHsarabun']],
            'default_font_size' => 12,
            'default_font' => 'thsarabun',
            'mode' => 'utf-8',
            'format' => 'A4',
            'margin_top' => 14,
            'margin_left' => 25,
            'margin_right' => 25,
            'margin_bottom' => 0,
            'margin_footer' => 5,
        ]);

        //  $mpdf->SetHTMLFooter($footer);
        $mpdf->WriteHTML($html);
        $mpdf->Output();

    }
    protected function findModel($id)
    {
        if (($model = MateOrder::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
