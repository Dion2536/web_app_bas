<?php

namespace app\modules\manager\controllers;

class ReportController extends \yii\web\Controller
{
    public $layout = 'main';
    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionOrder()
    {
        return $this->render('order');
    }
    public function actionMateOrder()
    {
        return $this->render('mate-order');
    }



}
