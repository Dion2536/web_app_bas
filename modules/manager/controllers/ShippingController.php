<?php

namespace app\modules\manager\controllers;

use app\models\Address;
use app\models\Order;
use app\models\Profile;
use http\Url;
use Mpdf\Mpdf;
use Yii;
use app\models\Shipping;
use app\models\ShippingSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ShippingController implements the CRUD actions for Shipping model.
 */
class ShippingController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public $layout = 'main';
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Shipping models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ShippingSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->where(['type_shipping'=>1])->orderBy('status');
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Shipping model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        if($model->load($post =Yii::$app->request->post())){
            $model->status = 2;
            $model->save();
            return $this->redirect(['shipping/view','id'=>$id]);
        }
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    public function actionSendSuccess($id)
    {
        $model = $this->findModel($id);
        if($model){
            $order =Order::findOne(['id'=>$model->order_id]);
            $order->status = 3 ; // send success
            $order->save();
            $model->status = 3;// send success
            $model->save();
        }
        return $this->redirect(['shipping/index','id'=>$id]);

    }

    public function actionReset($id)
    {
        $model = $this->findModel($id);
        if($model){
            $model->status = 1; // ค่าเรีม
            $model->date_send = null;
            $model->details = null;
            $model->save();
        }
        return $this->redirect(['shipping/view','id'=>$id]);

    }

    public function actionPrintBill($id)
    {

        $model = $this->findModel($id);
        $profile = Profile::findOne(['user_id'=>$model->user_id]);
        $address = Address::findOne(['user_id'=>$model->user_id]);
        $params = Yii::$app->params;
        $fontDirs = $params['defaultConfig']['fontDir'];
        $fontData = $params['defaultFontConfig']['fontdata'];

        $html = $this->renderPartial('print-bill',['profile'=>$profile,'address'=>$address,'model'=>$model ]);

        $mpdf = new Mpdf([
            'fontDir' => array_merge($fontDirs, [
                \yii\helpers\Url::base() . 'app_frontend/fonts/sarabun/',
            ]),
            'fontdata' => $fontData + ['thsarabun' => $params['SetFontTHsarabun']],
            'default_font_size' => 12,
            'default_font' => 'thsarabun',
            'mode' => 'utf-8',
            'format' => 'A4',
            'margin_top' => 14,
            'margin_left' => 25,
            'margin_right' => 25,
            'margin_bottom' => 0,
            'margin_footer' => 5,
        ]);

        //  $mpdf->SetHTMLFooter($footer);
        $mpdf->WriteHTML($html);
        $mpdf->Output();

    }

    /**
     * Creates a new Shipping model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Shipping();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Shipping model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Shipping model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Shipping model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Shipping the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Shipping::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
