<?php

namespace app\modules\manager\controllers;

use app\models\Order;
use app\models\User;
use yii\data\ActiveDataProvider;

class MemberController extends \yii\web\Controller
{
    public $layout = 'main';
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => User::find()->where(['roles'=>10]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionUseCheck($id){
        $model= User::findOne(['id'=>$id]);
        $model->status = 10;
        $model->save();
        return $this->redirect(['index']);
    }
    public function actionCancel($id){
        $model= User::findOne(['id'=>$id]);
        $model->status = 9;
        $model->save();
        return $this->redirect(['index']);
    }

}
