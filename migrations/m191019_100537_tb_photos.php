<?php

use yii\db\Migration;

/**
 * Class m191019_100537_tb_photos
 */
class m191019_100537_tb_photos extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('photos', [
            'id' => $this->primaryKey()->comment('รหัสรูปภาพ'),
            'name' => $this->text()->comment('ชื่อรูปภาพ'),
            'status_use' => $this->integer()->comment('ใช้รูปภาพ'),
            'product_id' => $this->integer()->comment('รหัสสินค้า'),
            'created_at' => $this->dateTime()->comment('วันที่สร้าง'),
            'updated_at' => $this->dateTime()->comment('วันที่แก้ไข'),
        ]);
    }

    public function down()
    {
        $this->dropTable('photos');
    }

}
