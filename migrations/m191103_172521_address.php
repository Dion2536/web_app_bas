<?php

use yii\db\Migration;

/**
 * Class m191103_172521_address
 */
class m191103_172521_address extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('address', [
            'id' => $this->primaryKey()->comment('รหัสที่อยู่'),
            'user_id' => $this->integer()->comment('รหัสผู้ใช้งาน'),
            'status' => $this->integer(2)->comment('สถานะ'),
            'full_name' => $this->string()->comment('ชื่อ-นามสกุล'),
            'phone' => $this->string(10)->comment('โทรศัพท์'),
            'address' => $this->string()->comment('ที่อยู่'),
            'created_at' => $this->dateTime()->comment('วันที่สร้าง'),
            'updated_at' => $this->dateTime()->comment('วันที่แก้ไข'),
        ]);
    }

    public function down()
    {
        $this->dropTable('address');
    }

}
