<?php

use yii\db\Migration;

/**
 * Class m191107_154009_bank_payment
 */
class m191107_154009_bank_payment extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('bank_payment', [
            'id'=>$this->primaryKey()->comment('รหัสธนาคาร'),
            'name'=>$this->string()->comment('ชื่อบัญชีธนาคาร'),
            'bank_name'=>$this->string()->comment('ชื่อธนาคาร'),
            'number_bank'=>$this->string()->comment('เลขบัญชี'),
            'bank_type'=>$this->integer()->comment('รูปแบบธนาคาร'),
            'status'=>$this->integer()->comment('สถานะ')
        ]);
    }

    public function down()
    {
        $this->dropTable('bank_payment');
    }

}
