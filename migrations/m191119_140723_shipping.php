<?php

use yii\db\Migration;

/**
 * Class m191119_140723_shipping
 */
class m191119_140723_shipping extends Migration
{


    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('shipping', [
            'id' => $this->primaryKey()->comment('รหัสส่งของ'),
            'order_id' => $this->integer()->comment('รหัสสั่งสินค้า'),
            'mate_order_id' => $this->integer()->comment('รหัสสั่งทำสินค้า'),
            'type_shipping' => $this->integer()->comment('ประเภทรายการ'),
            'user_id' => $this->integer()->comment('ชื่อผู้ส่ง'),
            'date_send' => $this->text()->comment('ระยะเวลาส่ง'),
            'status' => $this->integer()->comment('สถานะ'),
            'details' => $this->text()->comment('รายละเอียด'),
            'created_at' => $this->dateTime()->comment('วันที่สร้าง'),
            'updated_at' => $this->dateTime()->comment('วันที่แก้ไข'),
        ]);
    }

    public function down()
    {
        $this->dropTable('shipping');
    }

}
