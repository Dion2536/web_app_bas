<?php

use yii\db\Migration;

/**
 * Class m191012_005732_all_table
 */
class m191012_005732_all_table extends Migration
{


    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

        $this->createTable('types', [
            'id' => $this->primaryKey()->comment('รหัสประเภทสินค้า'),
            'name' => $this->string()->comment('ชื่อประเภทสินค้า'),
            'status' => $this->smallInteger()->comment('สถานะ'),
            'details' => $this->string()->comment('รายละเอียด'),
            'created_by' => $this->integer()->comment('ผู้สร้าง'),
            'updated_by' => $this->integer()->comment('ผู้แก้ไข'),
            'created_at' => $this->dateTime()->comment('วันที่สร้าง'),
            'updated_at' => $this->dateTime()->comment('วันที่แก้ไข'),
        ]);

        $this->createTable('product', [
            'id' => $this->primaryKey()->comment('รหัสสินค้า'),
            'name' => $this->string()->comment('ชื่อสินค้า'),
            'types_id' => $this->integer()->comment('ประเภทสินค้า'),
            'view' => $this->integer()->comment('จำนวนดู'),
            'status' => $this->smallInteger()->comment('สถานะ'),
            'price' => $this->float([8, 2])->comment('ราคา'),
            'price_old' => $this->float([8, 2])->comment('ราคาก่อนหน้า'),
            'details' => $this->text()->comment('รายละเอียดสินค้า'),
            'created_by' => $this->integer()->comment('ผู้สร้าง'),
            'updated_by' => $this->integer()->comment('ผู้แก้ไข'),
            'created_at' => $this->dateTime()->comment('วันที่สร้าง'),
            'updated_at' => $this->dateTime()->comment('วันที่แก้ไข'),
        ]);



        $this->createTable('movement', [
            'id' => $this->primaryKey()->comment('รหัสการเคลื่อนไหว'),
            'product_id' => $this->integer()->comment('รหัสสินค้า'),
            'type_movement' => $this->integer()->comment('ประเภทการเคลื่อนไหว'),
            'order_id' => $this->integer()->comment('รหัสสินค้า'),
            'amount' => $this->integer()->comment('จำนวน'),
            'details' => $this->string()->comment('รายละเอียด'),
            'status' => $this->smallInteger()->comment('สถานะ'),
            'created_by' => $this->integer()->comment('ผู้สร้าง'),
            'updated_by' => $this->integer()->comment('ผู้แก้ไข'),
            'created_at' => $this->dateTime()->comment('วันที่สร้าง'),
            'updated_at' => $this->dateTime()->comment('วันที่แก้ไข'),
        ]);
        $this->createTable('order', [
            'id' => $this->primaryKey()->comment('รหัสสั่งซื้อ'),
            'type_order' => $this->integer()->comment('รูปแบบสั่งซื้อ'),
            'status' => $this->smallInteger()->comment('สถานะ'),
            'created_by' => $this->integer()->comment('ผู้สร้าง'),
            'updated_by' => $this->integer()->comment('ผู้แก้ไข'),
            'created_at' => $this->dateTime()->comment('วันที่สร้าง'),
            'updated_at' => $this->dateTime()->comment('วันที่แก้ไข'),
        ]);
        $this->createTable('order_details', [
            'id' => $this->primaryKey()->comment('รหัสรายละเอียดสั่งซื้อ'),
            'order_id' => $this->integer()->comment('รหัสสั่งซื้อ'),
            'product_id' => $this->integer()->comment('รหัสสินค้า'),
            'status' => $this->smallInteger()->comment('สถานะ'),
            'amount' => $this->integer()->comment('จำนวน'),
            'created_by' => $this->integer()->comment('ผู้สร้าง'),
            'updated_by' => $this->integer()->comment('ผู้แก้ไข'),
            'created_at' => $this->dateTime()->comment('วันที่สร้าง'),
            'updated_at' => $this->dateTime()->comment('วันที่แก้ไข'),
        ]);

        $this->createTable('payment', [
            'id' => $this->primaryKey()->comment('รหัสชำระ'),
            'order_id' => $this->integer()->comment('รหัสสั่งสินค้า'),
            'type_payment' => $this->integer()->comment('ประเภทรายการ'),
            'mate_order_id' => $this->integer()->comment('รหัสสั่งทำสินค้า'),
            'bank_id' => $this->string()->comment('รหัสธนาคาร'),
            'status' => $this->smallInteger()->comment('สถานะ'),
            'check_time' => $this->string()->comment('เวลาโอน'),
            'remark' => $this->text()->comment('หมายเหตุ'),
            'created_by' => $this->integer()->comment('ผู้สร้าง'),
            'updated_by' => $this->integer()->comment('ผู้แก้ไข'),
            'created_at' => $this->dateTime()->comment('วันที่สร้าง'),
            'updated_at' => $this->dateTime()->comment('วันที่แก้ไข'),
        ]);

        $this->createTable('contact', [
            'id' => $this->primaryKey()->comment('รหัสติดต่อ'),
            'order_id' => $this->integer()->comment('รหัสสั่งซื้อ'),
            'address' => $this->string()->comment('ที่อยู่'),
            'status' => $this->smallInteger()->comment('สถานะ'),
            'created_by' => $this->integer()->comment('ผู้สร้าง'),
            'updated_by' => $this->integer()->comment('ผู้แก้ไข'),
            'created_at' => $this->dateTime()->comment('วันที่สร้าง'),
            'updated_at' => $this->dateTime()->comment('วันที่แก้ไข'),
        ]);
    }

    public function down()
    {
        $this->dropTable('types');
        $this->dropTable('product');
        $this->dropTable('movement');
        $this->dropTable('order');
        $this->dropTable('order_details');
        $this->dropTable('payment');
        $this->dropTable('contact');

    }

}
