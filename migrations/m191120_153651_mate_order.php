<?php

use yii\db\Migration;

/**
 * Class m191120_153651_mate_order
 */
class m191120_153651_mate_order extends Migration
{

    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('{{%mate_order}}', [
            'id' => $this->primaryKey()->comment('รหัสสั่งทำสินค้า'),
            'name' => $this->string()->comment('ชื่อสั่งทำสินค้า'),
            'details' => $this->text()->comment('รายละเอียด'),
            'user_id' => $this->integer()->comment('รหัสสมาชิก'),
            'deposit_price' => $this->integer()->comment('ราคามัดจำ'),
            'price' => $this->integer()->comment('ราคาเต็ม'),
            'time_length' => $this->string()->comment('ระยะเวลา'),
            'status' => $this->integer()->comment('สถานะ'),
            'created_at' => $this->dateTime()->comment('วันที่สร้าง'),
            'updated_at' => $this->dateTime()->comment('วันที่แก้ไข'),
        ]);
    }

    public function down()
    {
        $this->dropTable('mate_order');
    }

}
