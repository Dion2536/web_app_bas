<?php

use yii\db\Migration;

/**
 * Class m191103_094430_profile
 */
class m191103_094430_profile extends Migration
{


    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('profile', [
            'id' => $this->primaryKey()->comment('รหัสโปรไฟล์'),
            'user_id' => $this->integer()->comment('รหัสผู้ใช้งาน'),
            'member_code' => $this->integer()->comment('รหัสสมาชิก'),
            'full_name' => $this->string()->comment('ชื่อ-นามสกุล'),
            'phone' => $this->string(10)->comment('โทรศัพท์'),
            'sex' => $this->integer(2)->comment('เพศ'),
            'birthday' => $this->date()->comment('วันเกิด'),
            'created_at' => $this->dateTime()->comment('วันที่สร้าง'),
            'updated_at' => $this->dateTime()->comment('วันที่แก้ไข'),
        ]);
    }

    public function down()
    {
        $this->dropTable('profile');
    }

}
