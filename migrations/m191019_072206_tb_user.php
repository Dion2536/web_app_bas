<?php

use yii\db\Migration;

/**
 * Class m191019_072206_tb_user
 */
class m191019_072206_tb_user extends Migration
{
    public function up()
    {

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey()->comment('รหัสสมาชิก'),
            'username' => $this->string()->notNull()->unique()->comment('ชื่อผู้ใช้งาน'),
            'auth_key' => $this->string(32)->notNull()->comment('key auth รหัสผ่าน'),
            'verification_token' => $this->string()->comment('ยืนยัน verification_token'),
            'password_hash' => $this->string()->notNull()->comment('เข้ารหัสรูปแบบ string'),
            'password_reset_token' => $this->string()->unique()->comment('รหัส token ยืนยันการลืมรหัส'),
            'email' => $this->string()->unique()->comment('อีเมล์'),
            'status' => $this->smallInteger()->defaultValue(10)->comment('สถานะ'),
            'roles' => $this->smallInteger()->comment('สิทธื์ใช้งาน'),
            'created_at' => $this->integer()->notNull()->comment('วันที่สร้าง'),
            'updated_at' => $this->integer()->notNull()->comment('วันที่แก้ไข'),
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}
